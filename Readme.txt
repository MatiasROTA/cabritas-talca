Titulo del proyecto:"Pagina web Cabritas Talca"

AUTORES: Matias Rojas Tapia, Javier Rosales Rojas.

SISTEMA QUE SE TRABAJO: GNU/Linux.

FECHA: 17 de Agosto del 2020.

BREVE DESCRIPCIÓN: En el siguiente trabajo se realizo una pagina web para la empresa de cabritas Talca, emprendimiento talquino que se dedica a la venta de cabritas en la ciudad. En este trabajo se implemento una pagina web, en donde los usuarios pueden realizar pedidos a traves de la web y ademas se pueden ver los porductos y las novedades de la empresa.  

I) REQUISITOS:
Para la implementacion de la pagina web es necesario realizar las siguientes instalaciones en su computador:

    + Sistema operativo Linux (Ubuntu).
	-Modulo de php 7.3
	-PostgresSQL.
	-apache2
	-powerDesigner

Para poder visualizar nuestra pagina web en un browser, es necesario tener apache2, que es un es un servidor web HTTP de código abierto, para platafor-mas Unix (BSD, GNU/Linux, etc.) :

    # Descargar Apache 2.
	:$ sudo apt-get install apache2

    # Revisar si el servidor esta activo
	:$ systemctl status apache2.service

Para poder descargar postgres es necesario utilizar los siguientes comandos.
	# Cree la configuración del repositorio de archivos:
		sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $ (lsb_release -cs) -pgdg main"> /etc/apt/sources.list.d/pgdg.list'

	# Importe la clave de firma del repositorio:
		wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -

	# Actualice las listas de paquetes:
		sudo apt-get update

	# Instale la última versión de PostgreSQL.
	# Si desea una versión específica, use 'postgresql-12' o similar en lugar de 'postgresql':
	sudo apt-get -y instalar postgresql
	
	Los comandos a utilizar para la creacion de la base de datos es: 
	- psql -U postgres -W
		-create database db_cabritas_talca;
	- psql -U postgres -W db_cabritas_talca
	- psql -U postgres -W db_cabritas_talca < db_cabritas.sql
Con PowerDesigner lo que hacemos es obtener el codigo de nuestra base de datos, con la cual generamos el script de nuestra base de datos.

Pasos para poder instalar PHP:
	1 - Antes de ejecutar cualquier otra acción, actualize su sistema operativo con los comandos a continuación:
	-sudo apt-get update
	-sudo apt-get upgrade
	2 - Ejecute lo comando sudo apt-get install php para hacer la instalación del PHP 7.3. En seguida , acepte la instalación de los paquetes requeridos 

II) EJECUCIÓN DEL POYECTO:

Comando para la cracion de la base de datos:

	1) Una vez obtenido el script sql, iremos a nuestra terminal linux, para poder crear la base de datos del proyecto:
		-psql -U postgres -W
			-create database db_cabritas_talca;
	2) En el siguiente paso, debes volcar nuestro scrip en la base de datos recien generada, asi se traspasaran todos las tablas a nuestra base de datos:
		- psql -U postgres -W db_cabritas_talca < db_cabritas.sql     

	



    	  __^__                                      __^__
         ( ___ )------------------------------------( ___ )
          | / |    Desarrolladores:                  | \ |
          | / |     -Matias Rojas                    | \ |
          |___|     -Javier Rosales                  |___|
         (_____)------------------------------------(_____) 

