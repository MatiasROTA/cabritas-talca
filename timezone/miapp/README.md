# crear BD:

postgres=# create database db_miapp;

# Cargar esquema y datos iniciales.

$ psql -U postgres -W db_miapp < docs/db/db_miapp.sql
$ psql -U postgres -W db_miapp < docs/db/init.sql

# cambiar datos de acceso a la base de datos en conf.ini

# usuario por defecto del sistema:

usuario: 111111111
clave: admin
