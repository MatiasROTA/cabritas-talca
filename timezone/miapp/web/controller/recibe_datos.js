// Shorthand for $( document ).ready()
$(function() {
  
  $("#form").submit(function () {
    var user_name = $('#username').val().trim();
   
    
    $.ajax({
      type: "POST",
      dataType: 'json',
      url: "../lib/pagar.php",
      data: {username: user_name},
      async: "false",
      success: function (response) {
      
        if (response.success) {
          window.location=response.location;
        
        } else {
          showMessageAlert(response.msg);
        }
      }, error: function (e) {
        logout();
      }
    });
    
    $('#username').val('');
    
    return false;
  });
});
