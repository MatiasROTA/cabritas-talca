// Shorthand for $( document ).ready()
$(function(){
  
  $("#form-regist").submit(function () {
    var rut = $('#rut').val().trim(); 
    
    var idtipo = $('#idtipo').val().trim();  
    var nombre = $('#nombre').val().trim();
    var apep = $('#apep').val().trim();
    var apem = $('#apem').val().trim();
    var phone = $('#phone').val().trim();
    var email = $('#email').val().trim();
    var password = $('#password').val().trim();
	console.log(rut);
	console.log(nombre);
	console.log(apep);
	console.log(phone);
	console.log(email);
	console.log(phone);
	console.log(password); 
    console.log(apem);
    $.ajax({
      type: "POST",
      dataType: 'json',
      url: "../lib/register.php",
      data: {rut:rut, idtipo:idtipo, nombre:nombre, apep:apep, apem:apem, phone:phone, email:email, password:password },
      async: "false",
      success: function (response) {
      
        if (response.success) {
          window.location=response.location;
        
        } else {
          showMessageAlert(response.msg);
        }
      }, error: function (e) {
        logout();
      }
    });
    
    return false;
  });
});
