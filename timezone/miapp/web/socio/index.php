<?php
  session_start();
  require_once("../lib/common.php");
  validarSesion();
?>

<!DOCTYPE html>
<head>
  <?php 
    head(); 
  ?>
</head>
<meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Watch shop | eCommers</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="manifest" href="../../../site.webmanifest">
    <link rel="shortcut icon" type="../../../image/x-icon" href="../../../assets/img/favicon.ico">

        <!-- Site Icons -->
    <link rel="shortcut icon" href="#" type="../../../image/x-icon" />
    <link rel="apple-touch-icon" href="#" />

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../../../css/bootstrap.min.css">
    <!-- Pogo Slider CSS -->
    <link rel="stylesheet" href="../../../css/pogo-slider.min.css">
	<!-- Site CSS -->
    <link rel="stylesheet" href="../../../css/style.css">    
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="../../../css/responsive.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="../../../css/custom.css">

    <!-- CSS here -->
    <link rel="stylesheet" href="../../../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="../../../assets/css/flaticon.css">
    <link rel="stylesheet" href="../../../assets/css/slicknav.css">
    <link rel="stylesheet" href="../../../assets/css/animate.min.css">
    <link rel="stylesheet" href="../../../assets/css/magnific-popup.css">
    <link rel="stylesheet" href="../../../assets/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="../../../assets/css/themify-icons.css">
    <link rel="stylesheet" href="../../../assets/css/slick.css">
    <link rel="stylesheet" href="../../../assets/css/nice-select.css">
    <link rel="stylesheet" href="../../../assets/css/style.css">
	
<body>
 
  <?php 
    navbar(); 
  ?>
  <!--  -->
  <p></p>
  <!--  -->
<!-- END LOADER -->
 
    <header>
        <!-- Header Start -->
        <div class="header-area">
            <div class="main-header header-sticky">
                <div class="container-fluid">
                    <div class="menu-wrapper"  color=#efb810;>
                        <!-- Logo -->
                        <div class="logo">
                            <a href="inicio.html"><img src="../../../assets/img/logo/logo.png" alt=""></a>
                        </div>
                        <!-- Main-menu -->
                        <div class="main-menu d-none d-lg-block">
                            <nav>                                                
                                <ul id="navigation">  
                                    <li><a href="index.php">Inicio</a></li>
                                    
                                    <li><a href="shop.php">Sala de ventas</a></li>
                                    <li ><a href="#">Direcciones</a>
                                        <ul class="submenu">
                                            
                                            <li><a href="product_details.php"> Ingrese sus direcciones</a></li>
                                        </ul>
                                    </li>
                                   
 
                                </ul>
                            </nav>
                        </div>
                        <!-- Header Right -->
                        <div class="header-right">
                            <ul>
                                <li>
                                    
                                </li>
                                <li><a href="cart.php"><span class="flaticon-shopping-cart"></span></a> </li>
                            </ul>
                        </div>
                    </div>
                    <!-- Mobile Menu -->
                    <div class="col-12">
                        <div class="mobile_menu d-block d-lg-none"></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Header End -->
    </header>
    <main>
<!--
        ? slider Area Start 
-->
        <div class="slider-area ">
            <div class="slider-active">
                <!-- Single Slider -->
                <div class="single-slider slider-height d-flex align-items-center slide-bg">
                    <div class="container">
                        <div class="row justify-content-between align-items-center">
                            <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5">
                                <div class="hero__caption">
                                    <h1 data-animation="fadeInLeft" data-delay=".4s" data-duration="2000ms">Variedad de sabores</h1>
                                    <p data-animation="fadeInLeft" data-delay=".7s" data-duration="2000ms">Escoge tu cabrita favorita, tenemos variedad de precios y sabores, ademas reparto a domicilio gratis.</p>
                                </div>
                            </div>
                            <div class="col-xl-7 col-lg-7 col-md-7 col-sm-7 d-none d-sm-block">
                                <div class="hero__img" data-animation="bounceIn" data-delay=".4s">
                                    <img src="../../../assets/img/hero/watch.jpeg" alt="" class=" heartbeat">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Single Slider -->
                <div class="single-slider slider-height d-flex align-items-center slide-bg">
                    <div class="container">
                        <div class="row justify-content-between align-items-center">
                            <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5">
                                <div class="hero__caption">
                                    <h1 data-animation="fadeInLeft" data-delay=".4s" data-duration="2000ms">Celebra el dia del niño</h1>
                                    <p data-animation="fadeInLeft" data-delay=".7s" data-duration="2000ms">En este dia tan especial, disfruta nuestras ofertas especiales dia del niño, no te quedes sin la tuya.</p>
                                </div>
                            </div>
                            <div class="col-xl-7 col-lg-7 col-md-7 col-sm-7 d-none d-sm-block">
                                <div class="hero__img" data-animation="bounceIn" data-delay=".4s">
                                    <img src="../../../assets/img/hero/post.png" alt="" class=" heartbeat">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- slider Area End-->
        <!-- ? New Product Start -->
        <section class="new-product-area section-padding30">
            <div class="container">
                <!-- Section tittle -->
                <div class="row">
                    <div class="col-xl-12">
                        <div class="section-tittle mb-70">
                            <h2>Nuestros productos</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6">
                        <div class="single-new-pro mb-30 text-center">
                            <div class="product-img">
                                <img src="../../../assets/img/gallery/imagen12.jpeg" alt="">
                            </div>
                            <div class="product-caption">
                                <h3><a href="product_details.html">Cubeta XL</a></h3>
                                <span>$ 3.000</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6">
                        <div class="single-new-pro mb-30 text-center">
                            <div class="product-img">
                                <img src="../../../assets/img/gallery/imagen13.jpeg" alt="">
                            </div>
                            <div class="product-caption">
                                <h3><a href="product_details.html">Promocion cine en casa</a></h3>
                                <span>$ 8.000</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6">
                        <div class="single-new-pro mb-30 text-center">
                            <div class="product-img">
                                <img src="../../../assets/img/gallery/boxpop.jpeg" alt="">
                            </div>
                            <div class="product-caption">
                                <h3><a href="product_details.html">Promocion para parejas</a></h3>
                                <span>$ 5.000</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--  New Product End -->
        <!--? Gallery Area Start -->
        <div class="gallery-area">
            <div class="container-fluid p-0 fix">
                <div class="row">
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                        <div class="single-gallery mb-30">
                            <div class="gallery-img big-img" style="background-image: url(../../../images/imagen15.jpeg);"></div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                        <div class="single-gallery mb-15">
                            <div class="gallery-img big-img" style="background-image: url(../../../images/imagen7.jpeg);"></div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Gallery Area End -->
        <!--? Watch Choice  Start-->
        <div class="watch-area section-padding30">
            <div class="container">
                <div class="row align-items-center justify-content-between padding-130">
                    <div class="col-lg-5 col-md-6">
                        <div class="watch-details mb-40">
                            <h2>Minimarket Donde Mario</h2>
                            <p>Minimarket Donde Mario, es un almancen que se encuntra cerca de la universidad Autonoma, tiene los porductos mas ricos y frescos de la ciudad.</p>
               
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-10">
                        <div class="choice-watch-img mb-40">
                            <img src="../../../assets/img/gallery/imagen2.jpeg" alt="">
                        </div>
                    </div>
                </div>
                <div class="row align-items-center justify-content-between">
                    <div class="col-lg-6 col-md-6 col-sm-10">
                        <div class="choice-watch-img mb-40">
                            <img src="../../../assets/img/gallery/imagen3.jpeg" alt="">
                        </div>
                    </div>
                    <div class="col-lg-5 col-md-6">
                        <div class="watch-details mb-40">
                            <h2>Minimarket Rossy</h2>
                            <p>Minimarket Rossy, es un socio comercial, el cual vende nuestros productos en la poblacion Faustino Gonzalez.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
       
    </main>
	 <!-- JS here -->

    <script src="../../.././assets/js/vendor/modernizr-3.5.0.min.js"></script>
    <!-- Jquery, Popper, Bootstrap -->
    <script src="../../.././assets/js/vendor/jquery-1.12.4.min.js"></script>
    <script src="../../.././assets/js/popper.min.js"></script>
    <script src="../../.././assets/js/bootstrap.min.js"></script>
    <!-- Jquery Mobile Menu -->
    <script src="../../.././assets/js/jquery.slicknav.min.js"></script>

    <!-- Jquery Slick , Owl-Carousel Plugins -->
    <script src="../../.././assets/js/owl.carousel.min.js"></script>
    <script src="../../.././assets/js/slick.min.js"></script>

    <!-- One Page, Animated-HeadLin -->
    <script src="../../.././assets/js/wow.min.js"></script>
    <script src="../../.././assets/js/animated.headline.js"></script>
    <script src="../../.././assets/js/jquery.magnific-popup.js"></script>

    <!-- Scrollup, nice-select, sticky -->
    <script src="../../.././assets/js/jquery.scrollUp.min.js"></script>
    <script src="../../.././assets/js/jquery.nice-select.min.js"></script>
    <script src="../../.././assets/js/jquery.sticky.js"></script>
    
    <!-- contact js -->
    <script src="../../.././assets/js/contact.js"></script>
    <script src="../../.././assets/js/jquery.form.js"></script>
    <script src="../../.././assets/js/jquery.validate.min.js"></script>
    <script src="../../.././assets/js/mail-script.js"></script>
    <script src="../../.././assets/js/jquery.ajaxchimp.min.js"></script>
    
    <!-- Jquery Plugins, main Jquery -->	
    <script src="../../.././assets/js/plugins.js"></script>
    <script src="../../.././assets/js/main.js"></script>
    
    <!-- ALL JS FILES -->
	<script src="../../../js/jquery.min.js"></script>
	<script src="../../../js/popper.min.js"></script>
	<script src="../../../js/bootstrap.min.js"></script>
    <!-- ALL PLUGINS -->
	<script src="../../../js/jquery.magnific-popup.min.js"></script>
    <script src="../../../js/jquery.pogo-slider.min.js"></script> 
	<script src="../../../js/slider-index.js"></script>
	<script src="../../../js/smoothscroll.js"></script>
	<script src="../../../js/form-validator.min.js"></script>
    <script src="../../../js/contact-form-script.js"></script>
	<script src="../../../js/isotope.min.js"></script>	
	<script src="../../../js/images-loded.min.js"></script>	
    <script src="../../../js/custom.js"></script>
  <?php footer(); ?>
  <script src="../controller/index.js"></script>

</body>
</html>
