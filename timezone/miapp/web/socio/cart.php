<?php
  session_start();
  include '../lib/common.php';
  include '../lib/carrito.php';
  validarSesion();
  $conn = conectarBD();	

?>
<!doctype html>
<html lang="zxx">
<head>
	<?php 
		head(); 
	?>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Watch shop | eCommers</title>
  <meta name="description" content="">
  <meta name="socioport" content="width=device-width, initial-scale=1">
  <link rel="manifest" href="site.webmanifest">
  <link rel="shortcut icon" type="image/x-icon" href="../../../../../../assets/img/favicon.ico">

  <!-- CSS here -->
      <link rel="stylesheet" href="../../../assets/css/bootstrap.min.css">
      <link rel="stylesheet" href="../../../assets/css/owl.carousel.min.css">
      <link rel="stylesheet" href="../../../assets/css/flaticon.css">
      <link rel="stylesheet" href="../../../assets/css/slicknav.css">
      <link rel="stylesheet" href="../../../assets/css/animate.min.css">
      <link rel="stylesheet" href="../../../assets/css/magnific-popup.css">
      <link rel="stylesheet" href="../../../assets/css/fontawesome-all.min.css">
      <link rel="stylesheet" href="../../../assets/css/themify-icons.css">
      <link rel="stylesheet" href="../../../assets/css/slick.css">
      <link rel="stylesheet" href="../../../assets/css/nice-select.css">
      <link rel="stylesheet" href="../../../assets/css/style.css">
	  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
	  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
	  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>

</head>

<body>
  <?php 
	navbar(); 
	?>
    <header>
        <!-- Header Start -->
        <div class="header-area">
            <div class="main-header header-sticky">
                <div class="container-fluid">
                    <div class="menu-wrapper"  color=#efb810;>
                        <!-- Logo -->
                        <div class="logo">
                            <a href="inicio.html"><img src="../../../assets/img/logo/logo.png" alt=""></a>
                        </div>
                        <!-- Main-menu -->
                        <div class="main-menu d-none d-lg-block">
                            <nav>                                                
                                <ul id="navigation">  
                                    <li><a href="index.php">Inicio</a></li>
                                    
                                    <li><a href="shop.php">Sala de ventas</a></li>
                                    <li ><a href="#">Direcciones</a>
                                        <ul class="submenu">
                                            
                                            <li><a href="product_details.php"> Ingrese sus direcciones</a></li>
                                        </ul>
                                    </li>
                                   
 
                                </ul>
                            </nav>
                        </div>
                        <!-- Header Right -->
                        <div class="header-right">
                            <ul>
                                <li>
                                    
                                </li>
                                <li><a href="cart.php"><span class="flaticon-shopping-cart"></span></a> </li>
                            </ul>
                        </div>
                    </div>
                    <!-- Mobile Menu -->
                    <div class="col-12">
                        <div class="mobile_menu d-block d-lg-none"></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Header End -->
    </header>
      <!-- Hero Area Start-->
      <div class="slider-area ">
          <div class="single-slider slider-height2 d-flex align-items-center">
              <div class="container">
                  <div class="row">
                      <div class="col-xl-12">
                          <div class="hero-cap text-center">
                              <h2>Carro de compras</h2>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
      <!--================Cart Area =================-->
	<h3> Lista del carrito</h3>
					
		<?php if(!empty($_SESSION['CARRITO'])) { ?>
		<table class="table table-light table-bordered">
		<tbody>
			<tr>
				<th width="40"  > Descripion</th>
				<th width="15" class="text-center" > Cantidad</th>
				<th width="20" class="text-center"> Precio</th>
				<th width="20" class="text-center"> Total</th>
				<th width="5">--</th>
			</tr>
			<?php $total=0; ?>
			<?php foreach($_SESSION['CARRITO'] as $indice=>$producto){?>
			<tr>
				<td width="40" > <?php echo $producto['NOMBRE'];?></td>
				<td width="15" class="text-center"> <?php echo $producto['CANTIDAD'];?></td>
				<td width="20" class="text-center"> <?php echo $producto['PRECIO'];?></td>
				<td width="20" class="text-center"> <?php echo number_format($producto['PRECIO']* $producto['CANTIDAD'], 2);?></td>
				<td width="5"> 
					<form action="" method="post">
					<input type="hidden" name="id" id="id" value="<?php echo encriptar($producto['ID']); ?>">
					<button class="btn btn-danger" name="btnaccion" value="Eliminar" type="submit"> Eliminar</button>
					</form>
				 </td>
				
				
				
			</tr>
			<?php $total= $total+($producto['PRECIO']* $producto['CANTIDAD']); ?>
			<?php } ?>
			<tr>
				<td colspan="3" class="text-center" align="right"><h3>TOTAL</h3></td>
				<td align="right" class="text-center"><h3>$ <?php echo number_format($total,2);?>  </h3> </td>
				<td></td>
			
			</tr>
			<tr>
				<td colspan="5" > 
					
					
					<form action="../lib/pagarsocio.php" method="POST">
	
						
						<div class="col-md-6 form-group p_star">>
						  <label for="hora">hora de entrega</label>
						  <input type="time" class="form-control" id="hora" name="hora" value="13:45:00" >
						</div>
						<div class="col-md-6 form-group p_star">>
						  <label for="fecha">fecha de entrega</label>
						  <input type="date" class="form-control" id="fecha" name="fecha" placeholder="0">
						</div>

						<div class="col-md-6 form-group p_star">
							
								<label for="identrega"> Seleccione tipo de entrega:</label>
								<select class="form-control" id= "identrega" name = "identrega" >
									<option value= 1 >Despacho</option>
									<option value= 0 >Retiro en tienda</option>
								</select>
						</div>
												
						<div class="col-md-6 form-group p_star">>	
								<label for="fecha">Ingrese su direccion</label>
								<select id="direccion" name="direccion" class="form-control">
								<?php 
									$id_usu = $_SESSION['id_usu'];
									$stmt = $conn->prepare("select * from direccion where rut_usuario = :id_usu;");
									$stmt->bindValue(':id_usu', $id_usu);
									$stmt->execute();
									$array_session = $stmt->fetchAll(\PDO::FETCH_ASSOC);
									foreach($array_session as $producto){ 
									echo '<option value="'.$producto['id_direccion'].'">'.$producto['calle'].'</option>';
								 
								 } ?>
							</select>		
						</div>
						
						<div class="col-md-12 form-group p_star">
							<label for="descripcion">Inserte Descripcion </label>
							<textarea class="form-control" maxlength="200" id="descripcion" name="descripcion" rows="3"></textarea>
						</div>
						
						<button class="btn btn-primary btn-lg btn-block" type="submit" value="proceder" name="btnaccion" >Realizar encargo >> </button>
					</form>			
				</td>
				
			</tr>
		
		</tbody>
	</table>
	<?php } else{ ?>
		<div class="alert alert-success"> No existen objetos ingresados en el carrito</div>
	<?php }  ?>
	<div class="col-lg-6 col-md-6">
		
	</div>
      
      <!--================End Cart Area =================-->
  </main>>
  
  
  <!-- JS here -->

  <script src="../../.././assets/js/vendor/modernizr-3.5.0.min.js"></script>
  <!-- Jquery, Popper, Bootstrap -->
  <script src="../../.././assets/js/vendor/jquery-1.12.4.min.js"></script>
  <script src="../../.././assets/js/popper.min.js"></script>
  <script src="../../.././assets/js/bootstrap.min.js"></script>
  <!-- Jquery Mobile Menu -->
  <script src="../../.././assets/js/jquery.slicknav.min.js"></script>

  <!-- Jquery Slick , Owl-Carousel Plugins -->
  <script src="../../.././assets/js/owl.carousel.min.js"></script>
  <script src="../../.././assets/js/slick.min.js"></script>

  <!-- One Page, Animated-HeadLin -->
  <script src="../../.././assets/js/wow.min.js"></script>
  <script src="../../.././assets/js/animated.headline.js"></script>
  
  <!-- Scrollup, nice-select, sticky -->
  <script src="../../.././assets/js/jquery.scrollUp.min.js"></script>
  <script src="../../.././assets/js/jquery.nice-select.min.js"></script>
  <script src="../../.././assets/js/jquery.sticky.js"></script>
  <script src="../../.././assets/js/jquery.magnific-popup.js"></script>

  <!-- contact js -->
  <script src="../../../assets/js/contact.js"></script>
  <script src="../../../assets/js/jquery.form.js"></script>
  <script src="../../../assets/js/jquery.validate.min.js"></script>
  <script src="../../../assets/js/mail-script.js"></script>
  <script src="../../../assets/js/jquery.ajaxchimp.min.js"></script>
  
  <!-- Jquery Plugins, main Jquery -->	
  <script src="../../.././assets/js/plugins.js"></script>
  <script src="../../.././assets/js/main.js"></script>

</body>
</html>
