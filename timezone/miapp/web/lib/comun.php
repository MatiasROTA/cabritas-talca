<?php
if (session_status() == PHP_SESSION_NONE) {
  session_start();
}

setlocale (LC_TIME, "es_ES.utf8");

$file = "../../conf.ini";
$ini = parse_ini_file($file, false);

date_default_timezone_set($ini['timezone']);




function encriptar($string)
{
    $encrypt_method = $GLOBALS['ini']['encrypt_method'];
    $secret_key = $GLOBALS['ini']['secret_key'];
    $secret_iv = $GLOBALS['ini']['secret_iv'];
    $encrypted = openssl_encrypt($string, $encrypt_method, $secret_key, 0, $secret_iv);
    $encrypted = base64_encode($encrypted);
    return $encrypted;
}

function desencriptar($string)
{
    $encrypt_method = $GLOBALS['ini']['encrypt_method'];
    $secret_key = $GLOBALS['ini']['secret_key'];
    $secret_iv = $GLOBALS['ini']['secret_iv'];
    $decrypted = openssl_decrypt(base64_decode($string), $encrypt_method, $secret_key, 0, $secret_iv);
    return $decrypted;
}


/*
 * devuelve ID conexión a la DB
 */
function conectarBD () {
  $i = $GLOBALS['ini'];
  
  $host = $i["host"];
  $db = $i["db"];
  $user = $i["user"]; 
  $password = $i["password"];
  $port = $i["port"];
  
  // conectar a la base de datos
  try {
    $conn = new PDO('pgsql:host='.$host.';port='.$port.';dbname='.$db.';user='.$user.';password='.$password);
    return $conn;
    
  } catch (PDOException $e) {
    //echo $e->getMessage();
    return false;
  }
}

function ejecutarSQL ($stmt) {
  $res = array();
  $res["success"] = false;
  $res["msg"] = "Error SQL";
  $res["data"] = null;
  
  try {
    if ($stmt->execute()) {
      $res["data"] = $stmt->fetchAll(PDO::FETCH_ASSOC);
      $res["msg"] = "éxito";
      $res["success"] = true;
      
    } else {
      
      /* https://www.postgresql.org/docs/12/errcodes-appendix.html */
      $arrayError['23503'] = "El registro no se puede eliminar porque tiene información asociada.";
      
      $res["msg"] = "Error SQL, Contacte al soporte";      
    }

  } catch (PDOException $e) {
    $res["msg"] = $e->getMessage();
  }
  
  return $res;
}



?>
