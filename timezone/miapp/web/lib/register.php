<?php
session_start();
require('../lib/common.php');

$_SESSION['loggedin'] = false;
$_SESSION['id_usu'] = null;
$_SESSION['nemp'] = 0;
$success = false;
//~ $msg = "Error de usuario o clave";
$location = "../lib/logout.php";
$_SESSION['loggedinMIAPP'] = false;
$_SESSION['last_activity'] = 0;

$conn = conectarBD();

if ($conn) {
  
  
  if (isset($_POST['rut'], $_POST['idtipo'], $_POST['nombre'], $_POST['apep'], $_POST['apem'], $_POST['phone'], $_POST['email'], $_POST['password'])) {
    
    
    if ($_POST['rut']!="" and $_POST['idtipo']!="" and $_POST['nombre']!="" and $_POST['apep']!="" and $_POST['apem']!="" and $_POST['phone']!="" and $_POST['email']!="" and $_POST['password']!="") {
    
      $rut = $_POST['rut'];
      
      //~ if($_POST['idtipo'] == "cliente") {
		 //~ $idtipo = 1;
		 
		 
      //~ } 
      //~ if($_POST['idtipo'] == "socio comercial") {
		 
		 //~ $idtipo = 2; 
		 
		 //~ $location = "../../../inicio.html";
      //~ }
      
      $idtipo = $_POST['idtipo'];
      $nombre = $_POST['nombre'];
      $apep = $_POST['apep'];
      
      $apem = $_POST['apem'];
      $phone = $_POST['phone'];
      
      $email = $_POST['email'];
      $password = $_POST['password'];
      
      $encrypted_password = encriptar($password);         

      
      $sql = "insert into usuario (rut_usuario, id_tipo_cliente, nombre_usuario, apellido_paterno, apellido_materno, telefono_cliente, correo_electronico, password) values (:rut, :idtipo, :nombre, :apep, :apem, :phone, :email, :password);";
             
      $stmt = $conn->prepare($sql);
      
      $stmt->bindValue(':rut', $rut);
      $stmt->bindValue(':idtipo', $idtipo);

	  $stmt->bindValue(':nombre', $nombre);
      $stmt->bindValue(':apep', $apep);

      $stmt->bindValue(':apem', $apem);
      $stmt->bindValue(':phone', $phone);

	  $stmt->bindValue(':email', $email);
      $stmt->bindValue(':password', $encrypted_password);
	  $array_session = $stmt->fetchAll(\PDO::FETCH_ASSOC);
	  	
      if ($stmt->execute()) {
        $array_session = $stmt->fetchAll(\PDO::FETCH_ASSOC);
		$msg = "se ingresa a execute";
		$success = true;
	  }
      
    } else {
      $msg = "Todos los datos son requeridos1.";
    }
  } else {
	//~ $msg = "Todos los datos son requeridos1 .";	
  }
} else {
  $msg = "No puede conectar a la Base de Datos3.";
}

$jsonOutput = array('success' => $success, 'msg' => $msg, 'location'=> $location);
echo json_encode($jsonOutput);

?>
