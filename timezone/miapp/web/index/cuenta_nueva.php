<?php
require_once("../lib/common.php");
	?>

<!DOCTYPE html>
<html lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Cabritas Talca</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="manifest" href="site.webmanifest">
    <link rel="shortcut icon" type="image/x-icon" href="../../../assets/img/favicon.ico">

    <!-- CSS here -->
        <link rel="stylesheet" href="../../../assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="../../../assets/css/owl.carousel.min.css">
        <link rel="stylesheet" href="../../../assets/css/flaticon.css">
        <link rel="stylesheet" href="../../../assets/css/slicknav.css">
        <link rel="stylesheet" href="../../../assets/css/animate.min.css">
        <link rel="stylesheet" href="../../../assets/css/magnific-popup.css">
        <link rel="stylesheet" href="../../../assets/css/fontawesome-all.min.css">
        <link rel="stylesheet" href="../../../assets/css/themify-icons.css">
        <link rel="stylesheet" href="../../../assets/css/slick.css">
        <link rel="stylesheet" href="../../../assets/css/nice-select.css">
        <link rel="stylesheet" href="../../../assets/css/style.css">
</head>
<body>

    	<header>
        <!-- Header Start -->
        <div class="header-area">
            <div class="main-header header-sticky">
                <div class="container-fluid">
                    <div class="menu-wrapper"  color=#efb810;>
                        <!-- Logo -->
                        <div class="logo">
                            <a href="index.php"><img src="../../../assets/img/logo/logo.png" alt=""></a>
                        </div>
                        <!-- Main-menu -->
                        <div class="main-menu d-none d-lg-block">
                            <nav>                                                
                                <ul id="navigation">  
                                    <li><a href="../../../index.html">Inicio</a></li>
                                    <li><a href="../../../about.html">Nosotros</a></li>
                                    <li><a href="miapp/web/index/shop.php">Sala de ventas</a></li>

									<li><a 
									href="../../../contact.html">Manual</a></li>
                                </ul>
                            </nav>
                        </div>
                        <!-- Header Right -->
                        <div class="header-right">
                            <ul>
                                <li>
                                    <div class="nav-search search-switch">
                                        <span class="flaticon-search"></span>
                                    </div>
                                </li>
                                <li> <a href="login.php"><span class="flaticon-user"></span></a></li>
                                <li><a href="../../../cart.html"><span class="flaticon-shopping-cart"></span></a> </li>
                            </ul>
                        </div>
                    </div>
                    <!-- Mobile Menu -->
                    <div class="col-12">
                        <div class="mobile_menu d-block d-lg-none"></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Header End -->
    </header>

    <main>
        <!-- Hero Area Start-->
        <div class="slider-area ">
            <div class="single-slider slider-height2 d-flex align-items-center">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="hero-cap text-center">
                                <h2>Registro</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Hero Area End-->
        <!--================login_part Area =================-->
        <section class="login_part section_padding ">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-6 col-md-6">
                        <div class="login_part_form">
                            <div class="login_part_form_iner">
                                <h3>Bienvenido  ! <br>
                                    Porfavor Registrate</h3>
                                <form id="form-regist" name="form-regist" class="row contact_form" action="" method="POST" role="form-regist">
                                    
                                    <div class="col-md-12 form-group p_star">
										<label for="rut"> > Ingrese su RUT</label>
                                    
                                        <input id="rut" type="number" class="form-control" min="1000000" max="50000000" name="rut" placeholder="Rut (Sin puntos ni guion)" required>
                                    </div>
									
                                    <div class="col-md-12 form-group p_star">
                                        <label for="nombre"> > Ingrese su nombre</label>
                                     
                                        <input id="nombre" type="text" class="form-control" name="nombre" placeholder="Nombre" required>
                                    </div>
                                   
                                    <div class="col-md-12 form-group p_star">
										<label for="apep"> > Ingrese su apellido paterno</label>
                                     
                                        <input id="apep" type="text" class="form-control" name="apep"  placeholder="Apellido paterno" required>
                                    </div>
                                    
                                    <div class="col-md-12 form-group p_star">
                                        <label for="apem"> > Ingrese su apellido materno</label>
                                        
                                        <input id="apem" type="text" class="form-control" name="apem" placeholder="Apellido materno" required>
                                    </div>
                                    
                                    <div class="col-md-12 form-group p_star">
                                        <label for="phone"> > Ingrese su Telefono</label>
                                        
                                        <input id="phone" type="number" class="form-control" name="phone" placeholder="N de telefono" required>
                                    </div>

                                    <div class="col-md-12 form-group p_star">
                                        <label for="email"> > Ingrese su correo electronico</label>
                                        <input id="email" type="text" class="form-control" name="email" placeholder="ejemplo@ejemplo" required>
                                    </div>

									<div class="col-md-12 form-group p_star">
										
                                        <label for="password"> > Ingrese clave</label>
                                        <input id="password" type="password" class="form-control" name="password" placeholder="contrasenia" required>
                                    </div>
									<div class="col-md-12 form-group p_star">
										<label for="idtipo"> Seleccione tipo de usuario:</label>
										<select id="idtipo">
											<option value=1>Cliente</option>
											<option value=2>Socio</option>
										</select>
									</div>

                                    
                                    <div class="col-md-12 form-group">
                                        
                                        <div class="creat_account d-flex align-items-center">
                                        
                                        <button id="submit_login" type="submit"  class="btn_3">
                                            Registrarse
                                        </button>
                                        
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--================login_part end =================-->
    </main>

   <?php footer(); ?>
  <script src="../controller/register.js"></script>
	
</body>
</html>
