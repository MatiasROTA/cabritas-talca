<?php
require_once("../lib/common.php");
?>

<!DOCTYPE html>
<html lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Cabritas Talca</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="manifest" href="site.webmanifest">
    <link rel="shortcut icon" type="image/x-icon" href="../../../assets/img/favicon.ico">

    <!-- CSS here -->
        <link rel="stylesheet" href="../../../assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="../../../assets/css/owl.carousel.min.css">
        <link rel="stylesheet" href="../../../assets/css/flaticon.css">
        <link rel="stylesheet" href="../../../assets/css/slicknav.css">
        <link rel="stylesheet" href="../../../assets/css/animate.min.css">
        <link rel="stylesheet" href="../../../assets/css/magnific-popup.css">
        <link rel="stylesheet" href="../../../assets/css/fontawesome-all.min.css">
        <link rel="stylesheet" href="../../../assets/css/themify-icons.css">
        <link rel="stylesheet" href="../../../assets/css/slick.css">
        <link rel="stylesheet" href="../../../assets/css/nice-select.css">
        <link rel="stylesheet" href="../../../assets/css/style.css">
</head>
<body>

	<header>
        <!-- Header Start -->
        <div class="header-area">
            <div class="main-header header-sticky">
                <div class="container-fluid">
                    <div class="menu-wrapper"  color=#efb810;>
                        <!-- Logo -->
                        <div class="logo">
                            <a href="index.php"><img src="../../../assets/img/logo/logo.png" alt=""></a>
                        </div>
                        <!-- Main-menu -->
						<div class="main-menu d-none d-lg-block">
                            <nav>                                                
                                <ul id="navigation">  
                                    <li><a href="../../../index.html">Inicio</a></li>
                                    <li><a href="../../../about.html">Nosotros</a></li>
                                    <li><a href="miapp/web/index/shop.php">Sala de ventas</a></li>

                                    <li><a href="../../../contact.html">Manual</a></li>
                                </ul>
                            </nav>
                        </div>
                        <!-- Header Right -->
                        <div class="header-right">
                            <ul>
                                <li>
                                    <div class="nav-search search-switch">
                                        <span class="flaticon-search"></span>
                                    </div>
                                </li>
                                <li> <a href="login.php"><span class="flaticon-user"></span></a></li>
                                <li><a href="../../../cart.html"><span class="flaticon-shopping-cart"></span></a> </li>
                            </ul>
                        </div>
                    </div>
                    <!-- Mobile Menu -->
                    <div class="col-12">
                        <div class="mobile_menu d-block d-lg-none"></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Header End -->
    </header>
    
    <main>
    <main>
        <!-- Hero Area Start-->
        <div class="slider-area ">
            <div class="single-slider slider-height2 d-flex align-items-center">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="hero-cap text-center">
                                <h2>Ingreso</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Hero Area End-->
        <!--================login_part Area =================-->
        <section class="login_part section_padding ">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-6 col-md-6">
                        <div class="login_part_text text-center">
                            <div class="login_part_text_iner">
                                <h2>Quieres comprar?</h2>
                                <p>Resgistrate aqui y conoce todos los productos de cabritas Talca,
                                    podras disfrutar de increibles promociones y productos</p>
                                <a  class="btn_3" href = "cuenta_nueva.php" >REGISTRATE AQUI !</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="login_part_form">
                            <div class="login_part_form_iner">
                                <h3>Bienvenido  ! <br>
                                    Porfavor inicia seccion</h3>
                                <form id="form" name="form" class="row contact_form" action="" method="POST" role="form">
                                    <div class="col-md-12 form-group p_star">
                                        <input id="username" type="number" class="form-control" name="username" min="1000000" max="50000000"  placeholder="RUT (sin puntos ni guion)" required>
                                    </div>
                                    <div class="col-md-12 form-group p_star">
                                        <input id="password" type="password" class="form-control" name="password" placeholder="Clave" required>
                                            
                                    </div>
                                    <div class="col-md-12 form-group">
                                        
                                        <button id="submit_login" type="submit"  class="btn_3">
                                            Ingresar
                                        </button>
                                        
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--================login_part end =================-->
    </main>

   <?php footer(); ?>
  <script src="../controller/login.js"></script>
  	
</body>
</html>
