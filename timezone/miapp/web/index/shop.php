<?php		
  include '../../../base_de_datos.php';
  $conn = conectarBD();
?>
<!doctype html>
<html class="no-js" lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Watch shop | eCommers</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="manifest" href="site.webmanifest">
    <link rel="shortcut icon" type="image/x-icon" href="../../../assets/img/favicon.ico">

    <!-- CSS here -->
        <link rel="stylesheet" href="../../../assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="../../../assets/css/owl.carousel.min.css">
        <link rel="stylesheet" href="../../../assets/css/flaticon.css">
        <link rel="stylesheet" href="../../../assets/css/slicknav.css">
        <link rel="stylesheet" href="../../../assets/css/animate.min.css">
        <link rel="stylesheet" href="../../../assets/css/magnific-popup.css">
        <link rel="stylesheet" href="../../../assets/css/fontawesome-all.min.css">
        <link rel="stylesheet" href="../../../assets/css/themify-icons.css">
        <link rel="stylesheet" href="../../../assets/css/slick.css">
        <link rel="stylesheet" href="../../../assets/css/nice-select.css">
        <link rel="stylesheet" href="../../../assets/css/style.css">
		<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
</head>

<body>
    <!--? Preloader Start -->
    <div id="preloader-active">
        <div class="preloader d-flex align-items-center justify-content-center">
            <div class="preloader-inner position-relative">
                <div class="preloader-circle"></div>
                <div class="preloader-img pere-text">
                    <img src="../../../assets/img/logo/logo.png" alt="">
                </div>
            </div>
        </div>
    </div>
    <!-- Preloader Start -->
    <header>
        <!-- Header Start -->
        <div class="header-area">
            <div class="main-header header-sticky">
                <div class="container-fluid">
                    <div class="menu-wrapper">
                        <!-- Logo -->
                        <div class="logo">
                            <a href="inicio.html"><img src="../../../assets/img/logo/logo.png" alt=""></a>
                        </div>
                        <!-- Main-menu -->
						<div class="main-menu d-none d-lg-block">
                            <nav>                                                
                                <ul id="navigation">  
                                    <li><a href="../../../index.html">Inicio</a></li>
                                    <li><a href="../../../about.html">Nosotros</a></li>
                                    <li><a href="shop.php">Sala de ventas</a></li>
                                    
                                   
                                    </li>
                                    <li><a href="../../../contact.html">Manual</a></li>
                                </ul>
                            </nav>
                        </div>
                        <!-- Header Right -->
                        <div class="header-right">
                            <ul>
                                
                                <li> <a href="login.php"><span class="flaticon-user"></span></a></li>
                                <li><a href="../../../cart.html"><span class="flaticon-shopping-cart"></span></a> </li>
                            </ul>
                        </div>
                    </div>
                    <!-- Mobile Menu -->
                    <div class="col-12">
                        <div class="mobile_menu d-block d-lg-none"></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Header End -->
    </header>
    <main>
        <!-- Hero Area Start-->
        <div class="slider-area ">
            <div class="single-slider slider-height2 d-flex align-items-center">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="hero-cap text-center">
                                <h2>Conoce nuestros productos</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Hero Area End-->
                   
	      <div class="row">
			
			<?php 
				$stmt = $conn->prepare("select * from producto;");
				$stmt->execute();
				$array_session = $stmt->fetchAll(\PDO::FETCH_ASSOC);	
				$sentencia = $conn->prepare("select * from promocion where disponibilidad_promocion= true;");
				$sentencia->execute();
				$arreglo_session = $sentencia->fetchAll(\PDO::FETCH_ASSOC);
			
			?>
			<?php foreach($array_session as $producto){ ?>	
				<div class="col-3">
					<div class="card">
						<img class="card-img-top" title="Tituo del producto" alt="Titulo" src="<?php echo $producto['url_imagen']; ?>" alt="">
						<div class="card-body">
						<span><?php echo $producto['nombre_producto']; ?></span>
							<p class="card-text">Precio</p>
							<h5 class="card-title">$ <?php echo $producto['precio_producto_cliente']; ?> </h5>
							<p class="card-text">Descripcion</p>
							<h5 class="card-title"> Gramos: <?php echo $producto['gramos_productos_cliente']; ?> grs </h5>
							<form action="login.php" >
								<button class="btn btn-primary" type="submit" value="proceder" name="btnaccion" >PIDE YA!</button>				
							</form>
						</div>
					</div>
				</div>
			<?php } ?>	
			<?php foreach($arreglo_session as $producto){ ?>	
				<div class="col-3">
					<div class="card">
						<img class="card-img-top" title="Tituo del producto" alt="Titulo" src="<?php echo $producto['url_promocion']; ?>" alt="">
						<div class="card-body">
						<span><?php echo $producto['nombre_promocion']; ?></span>
							<p class="card-text">Precio</p>
							<h5 class="card-title">$ <?php echo $producto['precio_promocion']; ?> </h5>
							<p class="card-text">Descripcion</p>
							<h5 class="card-title"> Gramos: <?php echo $producto['calorias_promocion']; ?> grs </h5>
							<form action="login.php">			
							<button class="btn btn-primary" type="submit" >PIDE YA!</button>				
							</form>
						</div>
					</div>
				</div>
			<?php } ?>
		</div>
    </main>
    
    <!--? Search model Begin -->
    <div class="search-model-box">
        <div class="h-100 d-flex align-items-center justify-content-center">
            <div class="search-close-btn">+</div>
            <form class="search-model-form">
                <input type="text" id="search-input" placeholder="Searching key.....">
            </form>
        </div>
    </div>
    <!-- Search model end -->

<!-- JS here -->
    <!-- All JS Custom Plugins Link Here here -->
    <script src="../../../assets/js/vendor/modernizr-3.5.0.min.js"></script>
    <!-- Jquery, Popper, Bootstrap -->
    <script src="../../../assets/js/vendor/jquery-1.12.4.min.js"></script>
    <script src="../../../assets/js/popper.min.js"></script>
    <script src="../../../assets/js/bootstrap.min.js"></script>
    <!-- Jquery Mobile Menu -->
    <script src="../../../assets/js/jquery.slicknav.min.js"></script>

    <!-- Jquery Slick , Owl-Carousel Plugins -->
    <script src="../../../assets/js/owl.carousel.min.js"></script>
    <script src="../../../assets/js/slick.min.js"></script>

    <!-- One Page, Animated-HeadLin -->
    <script src="../../../assets/js/wow.min.js"></script>
    <script src="../../../assets/js/animated.headline.js"></script>
    <script src="../../../assets/js/jquery.magnific-popup.js"></script>

    <!-- Scroll up, nice-select, sticky -->
    <script src="../../../assets/js/jquery.scrollUp.min.js"></script>
    <script src="../../../assets/js/jquery.nice-select.min.js"></script>
    <script src="../../../assets/js/jquery.sticky.js"></script>
    
    <!-- contact js -->
    <script src="../../../assets/js/contact.js"></script>
    <script src="../../../assets/js/jquery.form.js"></script>
    <script src="../../../assets/js/jquery.validate.min.js"></script>
    <script src="../../../assets/js/mail-script.js"></script>
    <script src="../../../assets/js/jquery.ajaxchimp.min.js"></script>
    
    <!-- Jquery Plugins, main Jquery -->	
    <script src="../../../assets/js/plugins.js"></script>
    <script src="../../../assets/js/main.js"></script>
    
</body>
</html>
