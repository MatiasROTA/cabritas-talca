<?php
require_once("../lib/common.php");
?>

<!DOCTYPE html>
<head>
   <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Watch shop | eCommers</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="manifest" href="site.webmanifest">
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico">

    <!-- CSS here -->
        <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="../assets/css/owl.carousel.min.css">
        <link rel="stylesheet" href="../assets/css/flaticon.css">
        <link rel="stylesheet" href="../assets/css/slicknav.css">
        <link rel="stylesheet" href="../assets/css/animate.min.css">
        <link rel="stylesheet" href="../assets/css/magnific-popup.css">
        <link rel="stylesheet" href="../assets/css/fontawesome-all.min.css">
        <link rel="stylesheet" href="../assets/css/themify-icons.css">
        <link rel="stylesheet" href="../assets/css/slick.css">
        <link rel="stylesheet" href="../assets/css/nice-select.css">
        <link rel="stylesheet" href="../assets/css/style.css">



<?php 
  head(); 
?>
</head>

<body>
  <?php
    navbar();
  ?>
  <p></p>
  <!--  -->
  <div class="container-fluid">
    <!-- workspace -->

    <div class="row">
      <div align="center">
        <p></p>
<!--
        <img src="../images/logo.png" width="150px">
-->
        <h3 class="text-muted">
        <?php
          echo $GLOBALS['ini']['brand'] . " " . $GLOBALS['ini']['titulo'] . " " . $GLOBALS['ini']['version'];
        ?>
        </h3>
      </div>
    </div>
    
  </div>  
  
  <!--modals-->
  <div id="modal_login_popup" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">	
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Ingreso</h4>
        </div>
        <!--form-->
        <div id="div_form_login">
          <!--login form-->
          <form id="login_form" name="login_form" class="form-horizontal" role="form" action="" method="POST">
            <div class="modal-body">
              <div id="login_msg">
                <div id="info_login_msg" class="glyphicon glyphicon-chevron-right"></div>
                  <span id="text_login_msg">Credenciales</span>
              </div>
              <p></p>
              <div class="input-group" id="user_input">
                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                <input id="username" type="text" class="form-control" name="username"  placeholder="ej: 12345678K" required>
              </div>
              <p></p>
              <div class="input-group" id="user_pass">
                <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                <input id="password" type="password" class="form-control" name="password" placeholder="clave" required>
              </div>
            </div>
            <div class="modal-footer">
              <div>
                <button id="submitlogin" type="submit" class="btn btn-success btn-sm btn-block">Ingresar</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

  <main>
        <!-- Hero Area Start-->
        <div class="slider-area ">
            <div class="single-slider slider-height2 d-flex align-items-center">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="hero-cap text-center">
                                <h2>Ingreso</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Hero Area End-->
        <!--================login_part Area =================-->
        <section class="login_part section_padding ">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-6 col-md-6">
                        <div class="login_part_text text-center">
                            <div class="login_part_text_iner">
                                <h2>New to our Shop?</h2>
                                <p>There are advances being made in science and technology
                                    everyday, and a good example of this is the</p>
                                <a href="#" class="btn_3">Create an Account</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="login_part_form">
                            <div class="login_part_form_iner">
                                <h3>Bienvenido  ! <br>
                                    Porfavor inicia seccion</h3>
                                <form id="form" name="form" class="form-horizontal" role="form" action="" method="POST">
                                    
                                    <div class="input-group" id="user_input">
										<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
										<input id="username" type="text" class="form-control" name="username"  placeholder="ej: 12345678K" required>
									</div>
                                 <div class="input-group" id="user_pass">
										<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
										<input id="password" type="password" class="form-control" name="password" placeholder="clave" required>
									</div>
                                    <div class="col-md-12 form-group">
                                        <div class="creat_account d-flex align-items-center">
                                            <input type="checkbox" id="f-option" name="selector">
                                            <label for="f-option">Remember me</label>
                                        </div>
                                        <button id="submit_login" type="submit" class="btn btn-success btn-sm btn-block">Ingresar</button>
                                            Ingresar
                                        </button>
                                        <a class="lost_pass" href="#">forget password?</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--================login_part end =================-->
    </main>
  
  <?php footer(); ?>
  <script src="../controller/login.js"></script>
</body>
</html>
