<?php
session_start();
require_once("../lib/common.php");
validarSesion();

if (isset($_REQUEST['accion'])) {
  $conn = conectarBD();
  
  switch ($_REQUEST['accion']) {
    case 1:
      # select 
      seleccionar($conn);
      break;  
    case 2:
      # insertar
      insertar($conn);
      break;  
    case 3:
      # delete
      eliminar($conn);
      break;  
    case 4:
      # select one
      seleccionarUno($conn);
      break;
    case 5:
      # update
      actualizar($conn);
      break;  
    case 6:
      # select 
      seleccionarXEmpresa($conn);
      break; 
  }  
}

function seleccionar ($conn) {
  $id_usu = $_SESSION["id_usu"];
  
  $sql= "select id_resp, nom_resp, apep_resp, apem_resp, movil_resp, email_resp, nom_emp ".
        "from responsable join empresa on empresa.id_emp = responsable.id_emp where id_usu = :id_usu;";
  
  $stmt = $conn->prepare($sql);
  $stmt->bindValue(':id_usu', $id_usu);
    
  $res = ejecutarSQL($stmt);  
  echo json_encode(array("success"=>$res["success"], "msg"=>$res["msg"], "data"=>$res["data"]));
}

function seleccionarXEmpresa ($conn) {
  $id_emp = $_REQUEST["id_emp"];
  
  $sql= "select id_resp, nom_resp || ' ' || apep_resp || ' ' || apem_resp as responsable ".
        "from responsable where id_emp = :id_emp order by responsable asc;";
  
  $stmt = $conn->prepare($sql);
  $stmt->bindValue(':id_emp', $id_emp);
    
  $res = ejecutarSQL($stmt);  
  echo json_encode(array("success"=>$res["success"], "msg"=>$res["msg"], "data"=>$res["data"]));
}

function seleccionarUno ($conn) {
  $id_resp = $_REQUEST['id_resp'];
  
  $sql = "SELECT id_resp, nom_resp, apep_resp, apem_resp, movil_resp, email_resp, id_emp FROM responsable ".
         "WHERE id_resp = :id_resp;";
  $stmt = $conn->prepare($sql);
  $stmt->bindValue(':id_resp', $id_resp);

  $res = ejecutarSQL($stmt);  
  echo json_encode(array("success"=>$res["success"], "msg"=>$res["msg"], "data"=>$res["data"][0]));
}

function insertar($conn) {
  $id_emp = trim($_REQUEST['id_emp']);
  $nom_resp = trim($_REQUEST['nom_resp']);
  $apep_resp = trim($_REQUEST['apep_resp']);
  $apem_resp = trim($_REQUEST['apem_resp']);
  $movil_resp = trim($_REQUEST['movil_resp']);
  $email_resp = trim($_REQUEST['email_resp']);

  $sql = "INSERT INTO responsable (id_emp, nom_resp, apep_resp, apem_resp, movil_resp, email_resp) VALUES ".
         "(:id_emp, :nom_resp, :apep_resp, :apem_resp, :movil_resp, :email_resp);";

  $stmt = $conn->prepare($sql);
  $stmt->bindValue(':id_emp', $id_emp);
  $stmt->bindValue(':nom_resp', $nom_resp);
  $stmt->bindValue(':apep_resp', $apep_resp);
  $stmt->bindValue(':apem_resp', $apem_resp);
  $stmt->bindValue(':movil_resp', $movil_resp);
  $stmt->bindValue(':email_resp', $email_resp);

  $res = ejecutarSQL($stmt);
  
  echo json_encode(array("success"=>$res["success"], "msg"=>$res["msg"], "data"=>$res["data"])); 
}

function actualizar ($conn) {
  $id_resp = trim($_REQUEST['id_resp']);
  $id_emp = trim($_REQUEST['id_emp']);
  $nom_resp = trim($_REQUEST['nom_resp']);
  $apep_resp = trim($_REQUEST['apep_resp']);
  $apem_resp = trim($_REQUEST['apem_resp']);
  $movil_resp = trim($_REQUEST['movil_resp']);
  $email_resp = trim($_REQUEST['email_resp']);

  $sql = "UPDATE responsable SET id_emp = :id_emp, nom_resp = :nom_resp, apep_resp = :apep_resp, " .
         "apem_resp = :apem_resp, movil_resp = :movil_resp, email_resp = :email_resp WHERE id_resp = :id_resp;";
  
  $stmt = $conn->prepare($sql);
  $stmt->bindValue(':id_resp', $id_resp);
  $stmt->bindValue(':id_emp', $id_emp);
  $stmt->bindValue(':nom_resp', $nom_resp);
  $stmt->bindValue(':apep_resp', $apep_resp);
  $stmt->bindValue(':apem_resp', $apem_resp);
  $stmt->bindValue(':movil_resp', $movil_resp);
  $stmt->bindValue(':email_resp', $email_resp);
  
  $res = ejecutarSQL($stmt);  
  echo json_encode(array("success"=>$res["success"], "msg"=>$res["msg"], "data"=>$res["data"])); 
}


function eliminar($conn) {
  $id_resp = $_REQUEST['id_resp'];

  $sql = "delete from responsable where id_resp = :id_resp;";

  $stmt = $conn->prepare($sql);
  $stmt->bindValue(':id_resp', $id_resp);
  $res = ejecutarSQL($stmt);
  
  echo json_encode(array("success"=>$res["success"], "msg"=>$res["msg"], "data"=>$res["data"])); 
}

?>
