<!DOCTYPE html>
<?php
require_once("../lib/comun.php");
	$dataPoints = array();
	try{
			$conn = conectarBD();
			$sql="select nombre_producto, (precio_producto_cliente*stock_productos_cliente) as ganancia from producto order by nombre_producto asc";
			$stmt = $conn->prepare($sql);

			$stmt->execute(); 
			$data = $stmt->fetchAll(\PDO::FETCH_OBJ);

			foreach($data as $row){
				$etiqueta = $row->nombre_producto;
				array_push($dataPoints, array("label"=> $etiqueta, "y"=> $row->ganancia));
			}
			$conn = null;
	} 
	
	catch(\PDOException $ex){
		print($ex->getMessage());
	}
?>
<div id="chartContainer" style="height: 600px; width: 80%;"></div>
<script>
		window.onload = function(){

		var chart = new CanvasJS.Chart("chartContainer", {
			animationEnabled: true,
			theme: "dark1", // "light1", "light2", "dark1", "dark2"
			title:{
				text: "Grfico ganacias por producto cliente"
			},
			axisY: {
				title: "Ganancia esperada"
			},
			data: [{        
				type: "column",  
				showInLegend: true, 
				legendMarkerColor: "grey",
				legendText: "Nombre producto",     
				dataPoints: <?php echo json_encode($dataPoints, JSON_NUMERIC_CHECK);?>
			}]
		});
		chart.render();
		}
	</script>
	<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
	
</html>
