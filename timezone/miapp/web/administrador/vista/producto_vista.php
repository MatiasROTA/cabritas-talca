<?php
  session_start();
  require_once("../lib/common.php");
  validarSesion();
  
?>
<!doctype html>
<html class="no-js" lang="zxx">
<head>
	<?php 
		head(); 
	?>
</head>
	<meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Cabritas Talca</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<!--
    <link rel="manifest" href="../../../site.webmanifest">
-->
    <link rel="shortcut icon" type="../../../image/x-icon" href="../../../assets/img/favicon.ico">
    
    <link rel="shortcut icon" href="#" type="../../../image/x-icon" />
    <link rel="apple-touch-icon" href="#" />
    
    <!-- Site Icons -->
    <link rel="shortcut icon" href="#" type="../../../image/x-icon" />
    <link rel="apple-touch-icon" href="#" />

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../../../css/bootstrap.min.css">
    <!-- Pogo Slider CSS -->
    <link rel="stylesheet" href="../../../css/pogo-slider.min.css">
	<!-- Site CSS -->
    <link rel="stylesheet" href="../../../css/style.css">    
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="../../../css/responsive.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="../../../css/custom.css">
      
	    <!-- CSS here -->
    <link rel="stylesheet" href="../../../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="../../../assets/css/flaticon.css">
    <link rel="stylesheet" href="../../../assets/css/slicknav.css">
    <link rel="stylesheet" href="../../../assets/css/animate.min.css">
    <link rel="stylesheet" href="../../../assets/css/magnific-popup.css">
    <link rel="stylesheet" href="../../../assets/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="../../../assets/css/themify-icons.css">
    <link rel="stylesheet" href="../../../assets/css/slick.css">
    <link rel="stylesheet" href="../../../assets/css/nice-select.css">
    <link rel="stylesheet" href="../../../assets/css/style.css">
    <!-- archivos CSS, los estilos -->
    <link rel="stylesheet" href="../extras/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="../extras/bootstrap/normalize.css">
    <link rel="stylesheet" href="../extras/jquery/jquery.dataTables.min.css">
    <link rel="stylesheet" href="../extras/jquery/jquery-ui.min.css">
    <link rel="stylesheet" href="../extras/sweetalert2/sweetalert2.css">
    <link rel="stylesheet" href="../extras/font-awesome/css/all_fontawesome.css">
    <link href='../extras/fullcalendar/fullcalendar.min.css' rel='stylesheet' />
    <link href='../extras/fullcalendar/fullcalendar.print.min.css' rel='stylesheet' media='print' />


<body>
<?php 
navbar(); 
?>
<p></p>
<!--
     <div id="preloader">
		<div class="loader">
			<div class="box"></div>
			<div class="box"></div>
		</div>
    </div><!-- end loader -->
    <!--? Preloader Start -->
  <header>
        <!-- Header Start -->
        <div class="header-area">
            <div class="main-header header-sticky">
                <div class="container-fluid">
                    <div class="menu-wrapper"  color=#efb810;>
                        <!-- Logo -->
                        <div class="logo">
                            <a href="index.php"><img src="../../../assets/img/logo/logo.png" alt=""></a>
                        </div>
                        <!-- Main-menu -->
						<div class="main-menu d-none d-lg-block">
                             <nav>                                                
                                <ul id="navigation">  
                                    <li><a href="index.php">Inicio</a></li>
									<li><a href="producto_vista.php">Agregar productos</a>
                                        <ul class="submenu">
                                            <li><a href="grafico_ganancia.php">Grafico ganancias</a></li>
                                        </ul>
                                    </li>
									<li><a href="pedido_vista.php">Pedidos</a>
                                        <ul class="submenu">
                                            <li><a href="grafico_mejor_cliente.php">Grafico mejor cliente</a></li>
                                        </ul>
                                     </li>   
                                    <li><a href="usuario_vista.php">Usuarios</a></li>
                                </ul>
                            </nav>
                        </div>
                        <!-- Header Right -->
                        <div class="header-right">
                            <ul>
                                <li>
<!--
                                    <div class="nav-search search-switch">
                                        <span class="flaticon-search"></span>
                                    </div>
-->
                                </li>
<!--
                                <li> <a href="miapp/web/index/login.php"><span class="flaticon-user"></span></a></li>
                                <li><a href="cart.php"><span class="flaticon-shopping-cart"></span></a> </li>
-->
                            </ul>
                        </div>
                    </div>
                    <!-- Mobile Menu -->
                    <div class="col-12">
                        <div class="mobile_menu d-block d-lg-none"></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Header End -->
    </header>
    <main>
		<!-- workspace -->
    <div class="container-fluid">        
		<div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header bg-secondary">
                    Datos tabla prodcuto.
                    </div>

                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="id_table4" class="display table compact nowrap"></table>
                        </div>
                    </div>
                </div>
            </div>
        </div>        
    
    </div> <!-- fin container -->

         <!-- modals producto-->
    <div class="modal" id="modal-producto">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- header modal -->
                <div class="modal-header">
                    <h4 class="modal-title"><span id="titulo-modal-producto">Crear</span></h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
            
                <!-- body modal-->
                <div class="modal-body">
            
                    <div class="row">
                        <form id="form-producto" role="form" method="post" enctype="multipart/form-data">
<!--
                            INSERTAR NOMBRE EN LA TABLA PRODUCTO
-->
                            <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">

                        
                                <label for="nombre_producto">Nombre producto*</label>
                                <input type="text" class="form-control" id="nombre_producto" name="nombre_producto" data-required="true">
                            
                                <label for="precio_producto_cliente">Precio producto cliente</label>
                                <input type="text" class="form-control" id="precio_producto_cliente" name="precio_producto_cliente" data-required="true">
                            
                                <label for="gramos_productos_cliente">Gramos producto cliente</label>
                                <input type="text" class="form-control" id="gramos_productos_cliente" name="gramos_productos_cliente" data-required="true">
                            
                                <label for="stock_productos_cliente">Stock producto cliente</label>
                                <input type="text" class="form-control" id="stock_productos_cliente" name="stock_productos_cliente" data-required="true">
                            
                                <label for="precio_producto_socio">Precio producto socio</label>
                                <input type="text" class="form-control" id="precio_producto_socio" name="precio_producto_socio" data-required="true">
                            
                                <label for="gramos_producto_socio">Gramos producto socio</label>
                                <input type="text" class="form-control" id="gramos_producto_socio" name="gramos_producto_socio" data-required="true">
                            
                                <label for="stock_producto_socio">Stock producto socio</label>
                                <input type="text" class="form-control" id="stock_producto_socio" name="stock_producto_socio" data-required="true">
								
								<div>
									<label for="disponibilidad_socio">Diponibilidad socio</label>
									<select class="form-control" id= "disponibilidad_socio" name = "disponibilidad_socio" >
										<option value= 1 >si</option>
										<option value= 0 >no</option>
									</select>
								</div>
								
								<div>
									<label for="disponibilidad_cliente">Diponibilidad cliente</label>
									<select class="form-control" id= "disponibilidad_cliente" name = "disponibilidad_cliente" >
										<option value= 1 >si</option>
										<option value= 0 >no</option>
									</select>
                                </div>
                            
<!--
                                <label for="disponibilidad_cliente">Disponibilidad cliente</label>
                                <input type="text" class="form-control" id="disponibilidad_cliente" name="disponibilidad_cliente" data-required="true">
                                
-->
                                <label for="calorias_producto">Calorias producto</label>
                                <input type="text" class="form-control" id="calorias_producto" name="calorias_producto" data-required="true">
                                
                                <label for="url_imagen">Url imagen</label>
                                <input type="text" class="form-control" id="url_imagen" name="url_imagen" data-required="true">
                            
                            </div>
								
                            <div class="clearfix"></div>
                  
                            <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <label>* datos obligatorios</label>
                            </div>
                        </form>
                    </div>
                </div>
            
                <!-- footer modal -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-success btn-lg" id="btn-aceptar-producto">Aceptar</button>
                    <button type="button" class="btn btn-danger btn-lg" data-dismiss="modal">Cancelar</button>            
                </div>
            </div>
        </div>
	</div>

	
	
	<div id="chartContainer" style="height: 370px; width: 100%;"></div>
		
    </main>
    
</body>
    	
    <script src="../../.././assets/js/vendor/modernizr-3.5.0.min.js"></script>
    <!-- Jquery, Popper, Bootstrap -->
    <script src="../../.././assets/js/vendor/jquery-1.12.4.min.js"></script>
    <script src="../../.././assets/js/popper.min.js"></script>
    <script src="../../.././assets/js/bootstrap.min.js"></script>
    <!-- Jquery Mobile Menu -->
    <script src="../../.././assets/js/jquery.slicknav.min.js"></script>

    <!-- Jquery Slick , Owl-Carousel Plugins -->
    <script src="../../.././assets/js/owl.carousel.min.js"></script>
    <script src="../../.././assets/js/slick.min.js"></script>

    <!-- One Page, Animated-HeadLin -->
    <script src="../../.././assets/js/wow.min.js"></script>
    <script src="../../.././assets/js/animated.headline.js"></script>
    <script src="../../.././assets/js/jquery.magnific-popup.js"></script>

    <!-- Scrollup, nice-select, sticky -->
    <script src="../../.././assets/js/jquery.scrollUp.min.js"></script>
    <script src="../../.././assets/js/jquery.nice-select.min.js"></script>
    <script src="../../.././assets/js/jquery.sticky.js"></script>
    
    <!-- contact js -->
    <script src="../../.././assets/js/contact.js"></script>
    <script src="../../.././assets/js/jquery.form.js"></script>
    <script src="../../.././assets/js/jquery.validate.min.js"></script>
    <script src="../../.././assets/js/mail-script.js"></script>
    <script src="../../.././assets/js/jquery.ajaxchimp.min.js"></script>
    
    <!-- Jquery Plugins, main Jquery -->	
    <script src="../../.././assets/js/plugins.js"></script>
    <script src="../../.././assets/js/main.js"></script>
    
    <!-- ALL JS FILES -->
	<script src="../../../../js/jquery.min.js"></script>
	<script src="../../../../js/popper.min.js"></script>
	<script src="../../../../js/bootstrap.min.js"></script>
    <!-- ALL PLUGINS -->
	<script src="../../../../js/jquery.magnific-popup.min.js"></script>
    <script src="../../../../js/jquery.pogo-slider.min.js"></script> 
	<script src="../../../../js/slider-index.js"></script>
	<script src="../../../../js/smoothscroll.js"></script>
	<script src="../../../../js/form-validator.min.js"></script>
    <script src="../../../../js/contact-form-script.js"></script>
	<script src="../../../../js/isotope.min.js"></script>	
	<script src="../../../../js/images-loded.min.js"></script>	
    <script src="../../../../js/custom.js"></script>
    
    
    <!-- archivos javascripts -->
    <script src="../extras/modernizr/modernizr-2.8.3.min.js"></script>  
    <script src="../extras/jquery/jquery-3.2.1.min.js"></script>
    <script src="../extras/jquery/jquery-ui.min.js"></script>
    <script src="../extras/bootstrap/bootstrap.min.js"></script>
    <script src="../extras/jquery/jquery.dataTables.min.js"></script>
    <script src="../extras/datepicker/datepicker-es.js"></script>
    <script src="../extras/sweetalert2/sweetalert2.min.js"></script>
    <script src="../extras/fullcalendar/lib/moment.min.js"></script>
    <script src="../extras/fullcalendar/fullcalendar.min.js"></script>
    <script src="../extras/fullcalendar/locale-all.js"></script>
    
	<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
    
    <script src="../extras/jquery/buttons.colVis.min.js"></script> 
    <script src="../extras/jquery/dataTables.buttons.min.js"></script>
    <script src="../extras/jquery/pdfmake.min.js"></script>
    <script src="../extras/jquery/vfs_fonts.js"></script>
    <script src="../extras/jquery/buttons.html5.min.js"></script>   

    <script src="../controlador/producto.js"></script>
    
</html>    
   
