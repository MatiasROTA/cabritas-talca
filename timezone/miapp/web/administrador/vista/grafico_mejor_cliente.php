<!DOCTYPE html>
<?php
require_once("../lib/comun.php");
	$dataPoints = array();
	try{
			$conn = conectarBD();
			$sql="select nombre_usuario, count(pedido.rut_usuario) as mejor_cliente from usuario inner join pedido on pedido.rut_usuario = usuario.rut_usuario GROUP BY nombre_usuario";
			$stmt = $conn->prepare($sql);

			$stmt->execute(); 
			$data = $stmt->fetchAll(\PDO::FETCH_OBJ);

			foreach($data as $row){
				$etiqueta = $row->nombre_usuario;
				array_push($dataPoints, array("label"=> $etiqueta, "y"=> $row->mejor_cliente));
			}
			$conn = null;
	} 
	
	catch(\PDOException $ex){
		print($ex->getMessage());
	}
?>
<div id="chartContainer" style="height: 600px; width: 80%;"></div>
<script>
		window.onload = function(){

		var chart = new CanvasJS.Chart("chartContainer", {
			animationEnabled: true,
			theme: "dark1", // "light1", "light2", "dark1", "dark2"
			title:{
				text: "Grfico mejor cliente"
			},
			axisY: {
				title: "Veces que ha pedido un cliente"
			},
			data: [{        
				type: "column",  
				showInLegend: true, 
				legendMarkerColor: "grey",
				legendText: "Nombre cliente",     
				dataPoints: <?php echo json_encode($dataPoints, JSON_NUMERIC_CHECK);?>
			}]
		});
		chart.render();
		}
	</script>
	<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
	
</html>
