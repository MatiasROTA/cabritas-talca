<?php
require_once("../lib/comun.php");

if (isset($_REQUEST['accion'])) {
  switch ($_REQUEST['accion']) {     
    case 1:
      # select
      $conn = conectarBD();
      seleccionar ($conn);
      break;
    case 2:
      # insert
      $conn = conectarBD();
      insertar ($conn);
      break;       
    case 3:
      # select where = ?
      $conn = conectarBD();
      seleccionarUno ($conn);
      break;      
    case 4:
      # update where = ?
      $conn = conectarBD();
      actualizar ($conn);
      break;    
    case 5:
      # delete where = ?
      $conn = conectarBD();
      eliminar ($conn);
      break;
  }  
}

function insertar ($conn) {
  $nombre_producto = $_REQUEST['nombre_producto'];
  $precio_producto_cliente = $_REQUEST['precio_producto_cliente'];
  $gramos_productos_cliente = $_REQUEST['gramos_productos_cliente'];
  $stock_productos_cliente = $_REQUEST['stock_productos_cliente'];
  $precio_producto_socio = $_REQUEST['precio_producto_socio'];
  $gramos_producto_socio = $_REQUEST['gramos_producto_socio'];
  $stock_producto_socio = $_REQUEST['stock_producto_socio'];
  $disponibilidad_socio = $_REQUEST['disponibilidad_socio'];
  $disponibilidad_cliente = $_REQUEST['disponibilidad_cliente'];
  $calorias_producto = $_REQUEST['calorias_producto'];
  $url_imagen = $_REQUEST['url_imagen'];
  
  $sql= "insert into producto (nombre_producto, precio_producto_cliente,  gramos_productos_cliente, stock_productos_cliente, ". 
  "precio_producto_socio, gramos_producto_socio, stock_producto_socio, disponibilidad_socio, disponibilidad_cliente, calorias_producto, url_imagen) values ". 
  "(:nombre_producto, :precio_producto_cliente,  :gramos_productos_cliente, :stock_productos_cliente, :precio_producto_socio, ". 
  ":gramos_producto_socio, :stock_producto_socio, :disponibilidad_socio, :disponibilidad_cliente, :calorias_producto, :url_imagen )";
  
  $stmt = $conn->prepare($sql);
  $stmt->bindValue(':nombre_producto', $nombre_producto);
  $stmt->bindValue(':precio_producto_cliente', $precio_producto_cliente); 
  $stmt->bindValue(':gramos_productos_cliente', $gramos_productos_cliente); 
  $stmt->bindValue(':stock_productos_cliente', $stock_productos_cliente);
  $stmt->bindValue(':precio_producto_socio', $precio_producto_socio);
  $stmt->bindValue(':gramos_producto_socio', $gramos_producto_socio);
  $stmt->bindValue(':stock_producto_socio', $stock_producto_socio);
  $stmt->bindValue(':disponibilidad_socio', $disponibilidad_socio);
  $stmt->bindValue(':disponibilidad_cliente', $disponibilidad_cliente);
  $stmt->bindValue(':calorias_producto', $calorias_producto);
  $stmt->bindValue(':url_imagen', $url_imagen); 
    
  $res = ejecutarSQL($stmt);  
  echo json_encode(array("success"=>$res["success"], "msg"=>$res["msg"], "data"=>$res["data"][0]));
}

function actualizar ($conn) {
  $id_producto = $_REQUEST['id_producto'];
  $nombre_producto = $_REQUEST['nombre_producto'];
  $precio_producto_cliente = $_REQUEST['precio_producto_cliente'];
  $gramos_productos_cliente = $_REQUEST['gramos_productos_cliente'];
  $stock_productos_cliente = $_REQUEST['stock_productos_cliente'];
  $precio_producto_socio = $_REQUEST['precio_producto_socio'];
  $gramos_producto_socio = $_REQUEST['gramos_producto_socio'];
  $stock_producto_socio = $_REQUEST['stock_producto_socio'];
  $disponibilidad_socio = $_REQUEST['disponibilidad_socio'];
  $disponibilidad_cliente = $_REQUEST['disponibilidad_cliente'];
  $calorias_producto = $_REQUEST['calorias_producto'];
  $url_imagen = $_REQUEST['url_imagen'];
  
  $sql= "update producto set  nombre_producto = :nombre_producto, precio_producto_cliente = :precio_producto_cliente, gramos_productos_cliente = :gramos_productos_cliente, stock_productos_cliente = :stock_productos_cliente, precio_producto_socio = :precio_producto_socio, gramos_producto_socio = :gramos_producto_socio, stock_producto_socio = :stock_producto_socio, disponibilidad_socio = :disponibilidad_socio, disponibilidad_cliente = :disponibilidad_cliente, calorias_producto = :calorias_producto, url_imagen = :url_imagen where id_producto = :id_producto";
  
  $stmt = $conn->prepare($sql);
  $stmt->bindValue(':id_producto', $id_producto);
  $stmt->bindValue(':nombre_producto', $nombre_producto);
  $stmt->bindValue(':precio_producto_cliente', $precio_producto_cliente);
  $stmt->bindValue(':gramos_productos_cliente', $gramos_productos_cliente);
  $stmt->bindValue(':stock_productos_cliente', $stock_productos_cliente); 
  $stmt->bindValue(':precio_producto_socio', $precio_producto_socio);
  $stmt->bindValue(':gramos_producto_socio', $gramos_producto_socio);
  $stmt->bindValue(':stock_producto_socio', $stock_producto_socio);
  $stmt->bindValue(':disponibilidad_socio', $disponibilidad_socio);
  $stmt->bindValue(':disponibilidad_cliente', $disponibilidad_cliente);
  $stmt->bindValue(':calorias_producto', $calorias_producto);
  $stmt->bindValue(':url_imagen', $url_imagen);   
    
  $res = ejecutarSQL($stmt);  
  echo json_encode(array("success"=>$res["success"], "msg"=>$res["msg"], "data"=>$res["data"][0]));
}

function seleccionar ($conn) {
  $sql= "select id_producto, nombre_producto, precio_producto_cliente,  gramos_productos_cliente, stock_productos_cliente, precio_producto_socio, gramos_producto_socio, stock_producto_socio, disponibilidad_socio, disponibilidad_cliente, calorias_producto, url_imagen from producto order by nombre_producto asc";
  $stmt = $conn->prepare($sql);
    
  $res = ejecutarSQL($stmt);  
  echo json_encode(array("success"=>$res["success"], "msg"=>$res["msg"], "data"=>$res["data"]));
}

function seleccionarUno ($conn) {
  $id_producto = $_REQUEST['id_producto'];
  $sql= "select * from producto where id_producto = :id_producto";
  
  $stmt = $conn->prepare($sql);
  $stmt->bindValue(':id_producto', $id_producto);  
    
  $res = ejecutarSQL($stmt);  
  echo json_encode(array("success"=>$res["success"], "msg"=>$res["msg"], "data"=>$res["data"][0]));
}

function eliminar ($conn) {
  $id_producto = $_REQUEST['id_producto'];
  $sql= "delete from producto where id_producto = :id_producto";
  $stmt = $conn->prepare($sql);
  $stmt->bindValue(':id_producto', $id_producto);  
  $res = ejecutarSQL($stmt);  
  echo json_encode(array("success"=>$res["success"], "msg"=>$res["msg"], "data"=>$res["data"]));
}

?>
