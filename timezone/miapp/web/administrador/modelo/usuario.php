<?php
require_once("../lib/comun.php");

/*
  
create table especialidad (
    id_espec serial primary key,
    nom_espec varchar(100) not null
);
 
 */ 

if (isset($_REQUEST['accion'])) {
  switch ($_REQUEST['accion']) {     
    case 1:
      # select
      $conn = conectarBD();
      seleccionar ($conn);
      break;
    case 2:
      # insert
      $conn = conectarBD();
      insertar ($conn);
      break;       
    case 3:
      # select where = ?
      $conn = conectarBD();
      seleccionarUno ($conn);
      break;      
    case 4:
      # update where = ?
      $conn = conectarBD();
      actualizar ($conn);
      break;    
    case 5:
      # delete where = ?
      $conn = conectarBD();
      eliminar ($conn);
      break;
  }  
}

function insertar ($conn) {
  $rut_usuario = $_REQUEST['rut_usuario'];
  $id_tipo_cliente = $_REQUEST['id_tipo_cliente'];
  $nombre_usuario = $_REQUEST['nombre_usuario'];
  $apellido_paterno = $_REQUEST['apellido_paterno'];
  $apellido_materno = $_REQUEST['apellido_materno'];
  $telefono_cliente = $_REQUEST['telefono_cliente'];
  $correo_electronico = $_REQUEST['correo_electronico'];
  $password = $_REQUEST['password'];
  
  $sql= "insert into usuario (rut_usuario, id_tipo_cliente, nombre_usuario, apellido_paterno, apellido_materno, telefono_cliente, ".
   "correo_electronico, password ) values (:rut_usuario, :id_tipo_cliente, :nombre_usuario, :apellido_paterno, :apellido_materno, ".
   ":telefono_cliente, :correo_electronico, :password)";
  
  $stmt = $conn->prepare($sql);
  $stmt->bindValue(':rut_usuario', $rut_usuario);
  $stmt->bindValue(':id_tipo_cliente', $id_tipo_cliente);
  $stmt->bindValue(':nombre_usuario', $nombre_usuario);
  $stmt->bindValue(':apellido_paterno', $apellido_paterno);
  $stmt->bindValue(':apellido_materno', $apellido_materno);
  $stmt->bindValue(':telefono_cliente', $telefono_cliente);
  $stmt->bindValue(':correo_electronico', $correo_electronico);
  $stmt->bindValue(':password', $password);  
    
  $res = ejecutarSQL($stmt);  
  echo json_encode(array("success"=>$res["success"], "msg"=>$res["msg"], "data"=>$res["data"][0]));
}

function actualizar ($conn) {
  $rut_usuario = $_REQUEST['rut_usuario'];
  $id_tipo_cliente = $_REQUEST['id_tipo_cliente'];
  $nombre_usuario = $_REQUEST['nombre_usuario'];
  $apellido_paterno = $_REQUEST['apellido_paterno'];
  $apellido_materno = $_REQUEST['apellido_materno'];
  $telefono_cliente = $_REQUEST['telefono_cliente'];
  $correo_electronico = $_REQUEST['correo_electronico'];
  
  $sql= "update usuario set id_tipo_cliente = :id_tipo_cliente, nombre_usuario = :nombre_usuario, ".
  "apellido_paterno = :apellido_paterno, apellido_materno = :apellido_materno, telefono_cliente = :telefono_cliente, ".
  "correo_electronico = :correo_electronico where rut_usuario = :rut_usuario";
  
  $stmt = $conn->prepare($sql);
  $stmt->bindValue(':rut_usuario', $rut_usuario);
  $stmt->bindValue(':id_tipo_cliente', $id_tipo_cliente);
  $stmt->bindValue(':nombre_usuario', $nombre_usuario);
  $stmt->bindValue(':apellido_paterno', $apellido_paterno);
  $stmt->bindValue(':apellido_materno', $apellido_materno);
  $stmt->bindValue(':telefono_cliente', $telefono_cliente);
  $stmt->bindValue(':correo_electronico', $correo_electronico); 
    
  $res = ejecutarSQL($stmt);  
  echo json_encode(array("success"=>$res["success"], "msg"=>$res["msg"], "data"=>$res["data"][0]));
}

function seleccionar ($conn) {
  $rut_usuario = $_REQUEST['rut_usuario'];
  $nombre_tipo_cliente = $_REQUEST['nombre_tipo_cliente'];
  $nombre_usuario = $_REQUEST['nombre_usuario'];
  $apellido_paterno = $_REQUEST['apellido_paterno'];
  $apellido_materno = $_REQUEST['apellido_materno'];
  $telefono_cliente = $_REQUEST['telefono_cliente'];
  $correo_electronico = $_REQUEST['correo_electronico'];
  $password = $_REQUEST['password'];
  
  
  $sql= "select rut_usuario, tipo_usuario.nombre_tipo_cliente, nombre_usuario, apellido_paterno, apellido_materno, telefono_cliente, correo_electronico, password from usuario inner join tipo_usuario on usuario.id_tipo_cliente = tipo_usuario.id_tipo_cliente order by nombre_usuario";
  $stmt = $conn->prepare($sql);
  
  $stmt->bindValue(':rut_usuario', $rut_usuario);
  $stmt->bindValue(':nombre_tipo_cliente', $nombre_tipo_cliente);
  $stmt->bindValue(':nombre_usuario', $nombre_usuario);
  $stmt->bindValue(':apellido_paterno', $apellido_paterno);
  $stmt->bindValue(':apellido_materno', $apellido_materno);
  $stmt->bindValue(':telefono_cliente', $telefono_cliente);
  $stmt->bindValue(':correo_electronico', $correo_electronico);
  $stmt->bindValue(':password', $password);  
    
  $res = ejecutarSQL($stmt);  
  echo json_encode(array("success"=>$res["success"], "msg"=>$res["msg"], "data"=>$res["data"]));
}

function seleccionarUno ($conn) {
  $rut_usuario = $_REQUEST['rut_usuario'];
  $sql= "select * from usuario where rut_usuario = :rut_usuario";
  
  $stmt = $conn->prepare($sql);
  $stmt->bindValue(':rut_usuario', $rut_usuario);  
    
  $res = ejecutarSQL($stmt);  
  echo json_encode(array("success"=>$res["success"], "msg"=>$res["msg"], "data"=>$res["data"][0]));
}

function eliminar ($conn) {
  $rut_usuario = $_REQUEST['rut_usuario'];
  $sql= "delete from usuario where rut_usuario = :rut_usuario";
  
  $stmt = $conn->prepare($sql);
  $stmt->bindValue(':rut_usuario', $rut_usuario);  
    
  $res = ejecutarSQL($stmt);  
  echo json_encode(array("success"=>$res["success"], "msg"=>$res["msg"], "data"=>$res["data"]));
}

?>
