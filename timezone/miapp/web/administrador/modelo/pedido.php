<?php
require_once("../lib/comun.php");

if (isset($_REQUEST['accion'])) {
  switch ($_REQUEST['accion']) {     
    case 1:
      # select
      $conn = conectarBD();
      seleccionar ($conn);
      break;
    case 2:
      # insert
      $conn = conectarBD();
      insertar ($conn);
      break;       
    case 3:
      # select where = ?
      $conn = conectarBD();
      seleccionarUno ($conn);
      break;      
    case 4:
      # update where = ?
      $conn = conectarBD();
      actualizar ($conn);
      break;    
    case 5:
      # delete where = ?
      $conn = conectarBD();
      eliminar ($conn);
      break;
  }  
}

function insertar ($conn) {
  $id_pedido = $_REQUEST['id_pedido'];
  $rut_usuario = $_REQUEST['rut_usuario'];
  $id_direccion = $_REQUEST['id_direccion'];
  $fecha_pedido = $_REQUEST['fecha_pedido'];
  $precio_total_pedido = $_REQUEST['precio_total_pedido'];
  $estado_pedido = $_REQUEST['estado_pedido'];
  $detalle_pedido = $_REQUEST['detalle_pedido'];
  $fecha_entrega_pedido = $_REQUEST['fecha_entrega_pedido'];
  $hora_entrega_pedido = $_REQUEST['hora_entrega_pedido'];
  $tipo_entrega = $_REQUEST['tipo_entrega'];
  
  $sql= "insert into pedido(id_pedido, rut_usuario, id_direccion, fecha_pedido, precio_total_pedido, estado_pedido, detalle_pedido, fecha_entrega_pedido, hora_entrega_pedido, tipo_entrega) values(:id_pedido, :rut_usuario, :id_direccion, :fecha_pedido, :precio_total_pedido, :estado_pedido, :detalle_pedido, :fecha_entrega_pedido, :hora_entrega_pedido, :tipo_entrega)";
  
  $stmt = $conn->prepare($sql);
  $stmt->bindValue(':id_pedido', $id_pedido);
  $stmt->bindValue(':rut_usuario', $rut_usuario);
  $stmt->bindValue(':id_direccion', $id_direccion); 
  $stmt->bindValue(':fecha_pedido', $fecha_pedido); 
  $stmt->bindValue(':precio_total_pedido', $precio_total_pedido);
  $stmt->bindValue(':estado_pedido', $estado_pedido);
  $stmt->bindValue(':detalle_pedido', $detalle_pedido);
  $stmt->bindValue(':fecha_entrega_pedido', $fecha_entrega_pedido);
  $stmt->bindValue(':hora_entrega_pedido', $hora_entrega_pedido);
  $stmt->bindValue(':tipo_entrega', $tipo_entrega);
    
  $res = ejecutarSQL($stmt);  
  echo json_encode(array("success"=>$res["success"], "msg"=>$res["msg"], "data"=>$res["data"][0]));
}

function actualizar ($conn) {
  $id_pedido = $_REQUEST['id_pedido'];
  $rut_usuario = $_REQUEST['rut_usuario'];
  $id_direccion = $_REQUEST['id_direccion'];
  $fecha_pedido = $_REQUEST['fecha_pedido'];
  $precio_total_pedido = $_REQUEST['precio_total_pedido'];
  $estado_pedido = $_REQUEST['estado_pedido'];
  $detalle_pedido = $_REQUEST['detalle_pedido'];
  $fecha_entrega_pedido = $_REQUEST['fecha_entrega_pedido'];
  $hora_entrega_pedido = $_REQUEST['hora_entrega_pedido'];
  $tipo_entrega = $_REQUEST['tipo_entrega'];
  
  $sql= "update pedido set  rut_usuario = :rut_usuario, id_direccion = :id_direccion, fecha_pedido = :fecha_pedido, precio_total_pedido = :precio_total_pedido, estado_pedido = :estado_pedido, detalle_pedido = :detalle_pedido, fecha_entrega_pedido = :fecha_entrega_pedido, hora_entrega_pedido = :hora_entrega_pedido, tipo_entrega = :tipo_entrega where id_pedido = :id_pedido";
  
  $stmt = $conn->prepare($sql);
  $stmt->bindValue(':id_pedido', $id_pedido);
  $stmt->bindValue(':rut_usuario', $rut_usuario);
  $stmt->bindValue(':id_direccion', $id_direccion); 
  $stmt->bindValue(':fecha_pedido', $fecha_pedido); 
  $stmt->bindValue(':precio_total_pedido', $precio_total_pedido);
  $stmt->bindValue(':estado_pedido', $estado_pedido);
  $stmt->bindValue(':detalle_pedido', $detalle_pedido);
  $stmt->bindValue(':fecha_entrega_pedido', $fecha_entrega_pedido);
  $stmt->bindValue(':hora_entrega_pedido', $hora_entrega_pedido);
  $stmt->bindValue(':tipo_entrega', $tipo_entrega);   
    
  $res = ejecutarSQL($stmt);  
  echo json_encode(array("success"=>$res["success"], "msg"=>$res["msg"], "data"=>$res["data"][0]));
}

function seleccionar ($conn) {
  $id_pedido = $_REQUEST['id_pedido'];
  $rut_usuario = $_REQUEST['rut_usuario'];
  $calle = $_REQUEST['calle'];
  $fecha_pedido = $_REQUEST['fecha_pedido'];
  $precio_total_pedido = $_REQUEST['precio_total_pedido'];
  $estado_pedido = $_REQUEST['estado_pedido'];
  $detalle_pedido = $_REQUEST['detalle_pedido'];
  $fecha_entrega_pedido = $_REQUEST['fecha_entrega_pedido'];
  $hora_entrega_pedido = $_REQUEST['hora_entrega_pedido'];
  $tipo_entrega = $_REQUEST['tipo_entrega'];
 
  $sql= "select id_pedido, usuario.rut_usuario, direccion.calle,  fecha_pedido, precio_total_pedido, estado_pedido, detalle_pedido, fecha_entrega_pedido, hora_entrega_pedido, tipo_entrega from pedido inner join usuario on pedido.rut_usuario = usuario.rut_usuario inner join direccion on pedido.id_direccion = direccion.id_direccion order by precio_total_pedido asc";
  $stmt = $conn->prepare($sql);
  
  $stmt = $conn->prepare($sql);
  $stmt->bindValue(':id_pedido', $id_pedido);
  $stmt->bindValue(':rut_usuario', $rut_usuario);
  $stmt->bindValue(':calle', $calle); 
  $stmt->bindValue(':fecha_pedido', $fecha_pedido); 
  $stmt->bindValue(':precio_total_pedido', $precio_total_pedido);
  $stmt->bindValue(':estado_pedido', $estado_pedido);
  $stmt->bindValue(':detalle_pedido', $detalle_pedido);
  $stmt->bindValue(':fecha_entrega_pedido', $fecha_entrega_pedido);
  $stmt->bindValue(':hora_entrega_pedido', $hora_entrega_pedido);
  $stmt->bindValue(':tipo_entrega', $tipo_entrega);   
    
  $res = ejecutarSQL($stmt);  
  echo json_encode(array("success"=>$res["success"], "msg"=>$res["msg"], "data"=>$res["data"]));
}

function seleccionarUno ($conn) {
  $id_pedido = $_REQUEST['id_pedido'];
  $sql= "select * from pedido where id_pedido = :id_pedido";
  
  $stmt = $conn->prepare($sql);
  $stmt->bindValue(':id_pedido', $id_pedido);  
    
  $res = ejecutarSQL($stmt);  
  echo json_encode(array("success"=>$res["success"], "msg"=>$res["msg"], "data"=>$res["data"][0]));
}

function eliminar ($conn) {
  $id_pedido = $_REQUEST['id_pedido'];
  $sql= "delete from pedido where id_pedido = :id_pedido";
  $stmt = $conn->prepare($sql);
  $stmt->bindValue(':id_pedido', $id_pedido);  
  $res = ejecutarSQL($stmt);  
  echo json_encode(array("success"=>$res["success"], "msg"=>$res["msg"], "data"=>$res["data"]));
}

?>
