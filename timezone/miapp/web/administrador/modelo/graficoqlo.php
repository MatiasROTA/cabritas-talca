<!DOCTYPE html>
<?php
require_once("../lib/comun.php");
	$dataPoints = array();
	try{
			$conn = conectarBD();
			$sql="select nombre_producto, (precio_producto_cliente*stock_productos_cliente) as ganancia from producto order by nombre_producto asc";
			$stmt = $conn->prepare($sql);

			$stmt->execute(); 
			$data = $stmt->fetchAll(\PDO::FETCH_OBJ);

			foreach($data as $row){
				$etiqueta = $row->nombre_producto;
				array_push($dataPoints, array("label"=> $etiqueta, "y"=> $row->ganancia));
			}
			echo $dataPoints;
			$conn = null;
	} 
	
	catch(\PDOException $ex){
		print($ex->getMessage());
	}
?>
<div id="chartContainer" style="height: 370px; width: 100%;"></div>
<script>
		window.onload = function(){

		var chart = new CanvasJS.Chart("chartContainer", {
			animationEnabled: true,
			theme: "dark2", // "light1", "light2", "dark1", "dark2"
			title:{
				text: "Top Oil Reserves"
			},
			axisY: {
				title: "Reserves(MMbbl)"
			},
			data: [{        
				type: "column",  
				showInLegend: true, 
				legendMarkerColor: "grey",
				legendText: "MMbbl = one million barrels",     
				dataPoints: <?php echo json_encode($dataPoints, JSON_NUMERIC_CHECK);?>
			}]
		});
		chart.render();
		}
	</script>
	<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
	
</html>
