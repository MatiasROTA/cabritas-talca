// Shorthand for $( document ).ready()
$(function() {  
    obtenerPedido();
});

function obtenerPedido() {
  var accion_agregar = "<button type='button' class='btn btn-success btn-xs' " +
                       "onclick='agregar();' title='Agregar'>" + 
                       "<i class='fas fa-plus'></i></button>";
  
  var table = $('#id_table11').dataTable({
	  dom: 'Bfrtip',
	  buttons:[{
				extend: 'excelHtml5',
				autoFilter: true,
				filename: 'Pedidos',
				text: 'Generar Excel',
				exportOptions: {columns: ':visible'}
			
	}],
    "columnDefs": [
      {"title": "Id_pedido", "targets": 0, "orderable": false, "className": "dt-body-center", "visible": false},
      {"title": "Rut usuario", "targets": 1, "orderable": true, "className": "dt-body-center"},
      {"title": "Direccion", "targets": 2, "orderable": true, "className": "dt-body-center"},
      {"title": "Fecha pedido", "targets": 3, "orderable": true, "className": "dt-body-center"},
      {"title": "Precio total pedido", "targets": 4, "orderable": true, "className": "dt-body-center"},
      {"title": "Estado pedido", "targets": 5, "orderable": true, "className": "dt-body-center"},
      {"title": "Detalle pedido", "targets": 6, "orderable": true, "className": "dt-body-center"},
      {"title": "Fecha entrega pedido", "targets": 7, "orderable": true, "className": "dt-body-center"},
      {"title": "Hora entrega pedido", "targets": 8, "orderable": true, "className": "dt-body-center"},
      {"title": "Tipo entrega", "targets": 9, "orderable": true, "className": "dt-body-center"},
      {"title": accion_agregar, "targets": 10, "orderable": false, "className": "dt-nowrap dt-right"},
    ],
    "searching": true,
    "search": {
      "regex": true,
      "smart": true
    },
    "scrollX": false,
    "order": [[1, "asc"]],
    "bDestroy": true,
    "deferRender": true,
    "language": {
      "url": "../language/es.txt"
    },
    "pageLength": 10,
    "bPaginate": true,
    "bLengthChange": true,
    "bFilter": true,
    "bInfo": true,
    "bAutoWidth": false
  });
  
  table.fnClearTable();
  
  $.ajax({
    url: '../modelo/pedido.php',
    data: {accion: 1},
    type: 'POST',
    dataType: 'json',
    async: true,
    success: function(response) {
      if (response.success) {
        var data = response.data;
        
        for(var i = 0; i < data.length; i++) {
          table.fnAddData([
            data[i]["id_pedido"],
            data[i]["rut_usuario"],
            data[i]["calle"],
            data[i]["fecha_pedido"],
            data[i]["precio_total_pedido"],
            data[i]["estado_pedido"],
            data[i]["detalle_pedido"],
            data[i]["fecha_entrega_pedido"],
            data[i]["hora_entrega_pedido"],
            data[i]["tipo_entrega"],
      
            "<button type='button' class='btn btn-warning btn-xs' onclick='editar(" + data[i]["id_pedido"] + ");' title='Editar'>"+
            "<i class='fas fa-edit'></i></button>&nbsp;" +
            "<button type='button' class='btn btn-danger btn-xs' onclick='eliminar(" + data[i]["id_pedido"] + ");' title='Eliminar'>"+
            "<i class='fas fa-trash'></i></button>"
          ]);
        }
      } else {
        swal('Error', response.msg[2], 'error');
      }      
    }, error: function(jqXHR, textStatus, errorThrown ) {
      swal('Error', textStatus + " " + errorThrown, 'error');
    }
  });
}

/* levanta el modal para ingresar datos */
function agregar() {
  $("#titulo-modal-pedido").html("Crear");
  document.getElementById("form-pedido").reset();
  $("#btn-aceptar-pedido").attr("onClick", "agregarBD()");
  $("#modal-pedido").modal("show");
}

/* agrega un registro a la base de datos */
function agregarBD() {
  val = validarFormularioPedido();
  if (val == false) return false;
  
  /* convierte el formulario en un string de parámetros */
  var form = $("#form-pedido").serialize();
  
  $.ajax({
    dataType: 'json',
    async: true,
    url: '../modelo/pedido.php?accion=2',
    data: form,
    success: function (response) {    
      if (response.success) {          
        $("#modal-pedido").modal("hide");
        obtenerPedido();
          
      } else {
        swal('Error', response.msg[2], 'error');
      }
    }, error: function (e) {
      swal('Error', e.responseText, 'error');
    }
  }); 
}

/* obtiene datos de una especialidad y los muestra en el modal */
function editar(id_pedido) {
  document.getElementById("form-pedido").reset();

  $.post("../modelo/pedido.php?accion=3", {id_pedido: id_pedido}, function(response) {    
    if (response.success) {
      $.each(response.data, function(index, value) {
        if ($("input[name="+index+"]").length && value) {
          $("input[name="+index+"]").val(value);
        } else if ($("select[name="+index+"]").length && value){
          $("select[name="+index+"]").val(value);
        }
      });
    
      $("#titulo-modal-pedido").html("Editar");
      $("#btn-aceptar-pedido").attr("onClick", "editarBD(" + id_pedido + ")");
      $("#modal-pedido").modal("show");
    } else {
      swal('Error', response.msg[2], 'error');
    }
  }, 'json');
}

/* actualiza los datos en la base de datos */
function editarBD(id_pedido) {
  val = validarFormularioPedido();
  
  if (val == false) return false;
  
  var form = $("#form-pedido").serialize();
  $.post("../modelo/pedido.php?accion=4&id_pedido=" + id_pedido, form, function(response) {
  
    if (response.success) {
      $("#modal-pedido").modal("hide");
      obtenerPedido();
    } else {
      swal('Error', response.msg[2], 'error');
    }
  }, 'json');
}

/* elimina un registro de la base de datos */
function eliminar(id_pedido) {
  swal({
    title: '¿Está seguro (a)?',
    text: "Esta operación no se puede revertir!",
    type: 'warning',
    showCancelButton: true,
  }).then(function () {
    $.ajax({
      dataType: 'json',
      async: true,
      url: '../modelo/pedido.php',
      data: {accion: 5, id_pedido: id_pedido},
      success: function (response) {    
        if (response.success) {          
          obtenerPedido();
        } else {
          swal('Error', response.msg[2], 'error');
        }
      }, error: function (e) {
        swal('Error', e.responseText, 'error');
      }
    });    
  });
}

/* valida que los datos obligatorios tengan algún valor */
function validarFormularioPedido() {
  if ($('#id_pedido').val().trim().length<1) {
    swal('Atención', "El ID es requerido", 'info');
    return false;
  }
  
  return true;
}
