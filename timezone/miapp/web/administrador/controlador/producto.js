// Shorthand for $( document ).ready()
$(function() {  
    obtenerProducto();
});

function obtenerProducto() {
  var accion_agregar = "<button type='button' class='btn btn-success btn-xs' " +
                       "onclick='agregar();' title='Agregar'>" + 
                       "<i class='fas fa-plus'></i></button>";
  
  var table = $('#id_table4').dataTable({
	  dom: 'Bfrtip',
	  buttons:[{
			
			extend: 'pdfHtml5',
			download: 'open',
			messageTop: 'Lista de productos Cabritas Talca.',
			orientation: 'landscape',
			pageSize: 'LEGAL'
		
		},], 
    "columnDefs": [
      {"title": "Nombre Porducto", "targets": 0, "orderable": true, "className": "dt-body-center"},
      {"title": "Precio producto cliente", "targets": 1, "orderable": true, "className": "dt-body-center"},
      {"title": "Gramos producto cliente", "targets": 2, "orderable": true, "className": "dt-body-center"},
      {"title": "Stock producto cliente", "targets": 3, "orderable": true, "className": "dt-body-center"},
      {"title": "Precio producto socio", "targets": 4, "orderable": true, "className": "dt-body-center"},
      {"title": "Gramos producto socio", "targets": 5, "orderable": true, "className": "dt-body-center"},
      {"title": "Stock producto socio", "targets": 6, "orderable": true, "className": "dt-body-center"},
      {"title": "Disponibilidad socio", "targets": 7, "orderable": true, "className": "dt-body-center"},
      {"title": "Disponibilidad cliente", "targets": 8, "orderable": true, "className": "dt-body-center"},
      {"title": "Calorias producto", "targets": 9, "orderable": true, "className": "dt-body-center"},
      {"title": "Url imagen", "targets": 10, "orderable": true, "className": "dt-body-center"},
      {"title": accion_agregar, "targets": 11, "orderable": false, "className": "dt-nowrap dt-right"},
    ],
    "searching": true,
    "search": {
      "regex": true,
      "smart": true
    },
    "scrollX": false,
    "order": [[1, "asc"]],
    "bDestroy": true,
    "deferRender": true,
    "language": {
      "url": "../language/es.txt"
    },
    "pageLength": 10,
    "bPaginate": true,
    "bLengthChange": true,
    "bFilter": true,
    "bInfo": true,
    "bAutoWidth": false
  });
  
  table.fnClearTable();
  
  $.ajax({
    url: '../modelo/producto.php',
    data: {accion: 1},
    type: 'POST',
    dataType: 'json',
    async: true,
    success: function(response) {
      if (response.success) {
        var data = response.data;
        
        for(var i = 0; i < data.length; i++) {
          table.fnAddData([
            data[i]["nombre_producto"],
            data[i]["precio_producto_cliente"],
            data[i]["gramos_productos_cliente"],
            data[i]["stock_productos_cliente"],
            data[i]["precio_producto_socio"],
            data[i]["gramos_producto_socio"],
            data[i]["stock_producto_socio"],
            data[i]["disponibilidad_socio"],
            data[i]["disponibilidad_cliente"],
            data[i]["calorias_producto"],
            data[i]["url_imagen"],

            "<button type='button' class='btn btn-warning btn-xs' onclick='editar(" + data[i]["id_producto"] + ");' title='Editar'>"+
            "<i class='fas fa-edit'></i></button>&nbsp;" +
            "<button type='button' class='btn btn-danger btn-xs' onclick='eliminar(" + data[i]["id_producto"] + ");' title='Eliminar'>"+
            "<i class='fas fa-trash'></i></button>"
          ]);
        }
      } else {
        swal('Error', response.msg[2], 'error');
      }      
    }, error: function(jqXHR, textStatus, errorThrown ) {
      swal('Error', textStatus + " " + errorThrown, 'error');
    }
  });
}

/* levanta el modal para ingresar datos */
function agregar() {
  $("#titulo-modal-producto").html("Crear");
  document.getElementById("form-producto").reset();
  $("#btn-aceptar-producto").attr("onClick", "agregarBD()");
  $("#modal-producto").modal("show");
}

/* agrega un registro a la base de datos */
function agregarBD() {
  val = validarFormularioProducto();
  if (val == false) return false;
  
  /* convierte el formulario en un string de parámetros */
  var form = $("#form-producto").serialize();
  
  $.ajax({
    dataType: 'json',
    async: true,
    url: '../modelo/producto.php?accion=2',
    data: form,
    success: function (response) {    
      if (response.success) {          
        $("#modal-producto").modal("hide");
        obtenerProducto();
          
      } else {
        swal('Error', response.msg[2], 'error');
      }
    }, error: function (e) {
      swal('Error', e.responseText, 'error');
    }
  }); 
}

/* obtiene datos de una especialidad y los muestra en el modal */
function editar(id_producto) {
  document.getElementById("form-producto").reset();

  $.post("../modelo/producto.php?accion=3", {id_producto: id_producto}, function(response) {    
    if (response.success) {
      $.each(response.data, function(index, value) {
        if ($("input[name="+index+"]").length && value) {
          $("input[name="+index+"]").val(value);
        } else if ($("select[name="+index+"]").length && value){
          $("select[name="+index+"]").val(value);
        }
      });
    
      $("#titulo-modal-producto").html("Editar");
      $("#btn-aceptar-producto").attr("onClick", "editarBD(" + id_producto + ")");
      $("#modal-producto").modal("show");
    } else {
      swal('Error', response.msg[2], 'error');
    }
  }, 'json');
}

/* actualiza los datos en la base de datos */
function editarBD(id_producto) {
  val = validarFormularioProducto();
  
  if (val == false) return false;
  
  var form = $("#form-producto").serialize();
  $.post("../modelo/producto.php?accion=4&id_producto=" + id_producto, form, function(response) {
  
    if (response.success) {
      $("#modal-producto").modal("hide");
      obtenerProducto();
    } else {
      swal('Error', response.msg[2], 'error');
    }
  }, 'json');
}

/* elimina un registro de la base de datos */
function eliminar(id_producto) {
  swal({
    title: '¿Está seguro (a)?',
    text: "Esta operación no se puede revertir!",
    type: 'warning',
    showCancelButton: true,
  }).then(function () {
    $.ajax({
      dataType: 'json',
      async: true,
      url: '../modelo/producto.php',
      data: {accion: 5, id_producto: id_producto},
      success: function (response) {    
        if (response.success) {          
          obtenerProducto();
        } else {
          swal('Error', response.msg[2], 'error');
        }
      }, error: function (e) {
        swal('Error', e.responseText, 'error');
      }
    });    
  });
}

/* valida que los datos obligatorios tengan algún valor */
function validarFormularioProducto() {
  if ($('#nombre_producto').val().trim().length<1) {
    swal('Atención', "El Nombre es requerido", 'info');
    return false;
  }
  
  return true;
}
