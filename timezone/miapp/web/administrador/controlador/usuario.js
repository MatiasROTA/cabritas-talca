// Shorthand for $( document ).ready()
$(function() {  
    obtenerUsuario();
});

function obtenerUsuario() {
  var accion_agregar = "<button type='button' class='btn btn-success btn-xs' ";
  
  var table = $('#id_table6').dataTable({
    "columnDefs": [
      {"title": "Rut usuario", "targets": 0, "orderable": false, "className": "dt-body-center", "visible": true},
      {"title": "Tipo_usuario", "targets": 1, "orderable": true, "className": "dt-body-center"},
      {"title": "Nombre usuario", "targets": 2, "orderable": true, "className": "dt-body-center"},
      {"title": "Apellido paterno", "targets": 3, "orderable": true, "className": "dt-body-center"},
      {"title": "Apellido materno", "targets": 4, "orderable": true, "className": "dt-body-center"},
      {"title": "Telefono usuario", "targets": 5, "orderable": true, "className": "dt-body-center"},
      {"title": "Correo electronico", "targets": 6, "orderable": true, "className": "dt-body-center"},
      {"title": accion_agregar, "targets": 7, "orderable": false, "className": "dt-nowrap dt-right"},
    ],
    "searching": true,
    "search": {
      "regex": true,
      "smart": true
    },
    "scrollX": false,
    "order": [[1, "asc"]],
    "bDestroy": true,
    "deferRender": true,
    "language": {
      "url": "../language/es.txt"
    },
    "pageLength": 10,
    "bPaginate": true,
    "bLengthChange": true,
    "bFilter": true,
    "bInfo": true,
    "bAutoWidth": false
  });
  
  table.fnClearTable();
  
  $.ajax({
    url: '../modelo/usuario.php',
    data: {accion: 1},
    type: 'POST',
    dataType: 'json',
    async: true,
    success: function(response) {
      if (response.success) {
        var data = response.data;
        
        for(var i = 0; i < data.length; i++) {
          table.fnAddData([
            data[i]["rut_usuario"],
            data[i]["nombre_tipo_cliente"],
            data[i]["nombre_usuario"],
            data[i]["apellido_paterno"],
            data[i]["apellido_materno"],
            data[i]["telefono_cliente"],
            data[i]["correo_electronico"],
      
            "<button type='button' class='btn btn-warning btn-xs' onclick='editar(" + data[i]["rut_usuario"] + ");' title='Editar'>"+
            "<i class='fas fa-edit'></i></button>&nbsp;" +
            "<button type='button' class='btn btn-danger btn-xs' onclick='eliminar(" + data[i]["rut_usuario"] + ");' title='Eliminar'>"+
            "<i class='fas fa-trash'></i></button>"
          ]);
        }
      } else {
        swal('Error', response.msg[2], 'error');
      }      
    }, error: function(jqXHR, textStatus, errorThrown ) {
      swal('Error', textStatus + " " + errorThrown, 'error');
    }
  });
}

/* levanta el modal para ingresar datos */
//~ function agregar() {
  //~ $("#titulo-modal-usuario").html("Crear");
  //~ document.getElementById("form-usuario").reset();
  //~ $("#btn-aceptar-usuario").attr("onClick", "agregarBD()");
  //~ $("#modal-usuario").modal("show");
//~ }

/* agrega un registro a la base de datos */
//~ function agregarBD() {
  //~ val = validarFormularioUsuario();
  //~ if (val == false) return false;
  
  //~ /* convierte el formulario en un string de parámetros */
  //~ var form = $("#form-usuario").serialize();
  
  //~ $.ajax({
    //~ dataType: 'json',
    //~ async: true,
    //~ url: '../modelo/usuario.php?accion=2',
    //~ data: form,
    //~ success: function (response) {    
      //~ if (response.success) {          
        //~ $("#modal-usuario").modal("hide");
        //~ obtenerUsuario();
          
      //~ } else {
        //~ swal('Error', response.msg[2], 'error');
      //~ }
    //~ }, error: function (e) {
      //~ swal('Error', e.responseText, 'error');
    //~ }
  //~ }); 
//~ }

/* obtiene datos de una especialidad y los muestra en el modal */
function editar(rut_usuario) {
  document.getElementById("form-usuario").reset();

  $.post("../modelo/usuario.php?accion=3", {rut_usuario: rut_usuario}, function(response) {    
    if (response.success) {
      $.each(response.data, function(index, value) {
        if ($("input[name="+index+"]").length && value) {
          $("input[name="+index+"]").val(value);
        } else if ($("select[name="+index+"]").length && value){
          $("select[name="+index+"]").val(value);
        }
      });
    
      $("#titulo-modal-usuario").html("Editar");
      $("#btn-aceptar-usuario").attr("onClick", "editarBD(" + rut_usuario + ")");
      $("#modal-usuario").modal("show");
    } else {
      swal('Error', response.msg[2], 'error');
    }
  }, 'json');
}

/* actualiza los datos en la base de datos */
function editarBD(rut_usuario) {
  val = validarFormularioUsuario();
  
  if (val == false) return false;
  
  var form = $("#form-usuario").serialize();
  $.post("../modelo/usuario.php?accion=4&rut_usuario=" + rut_usuario, form, function(response) {
  
    if (response.success) {
      $("#modal-usuario").modal("hide");
      obtenerUsuario();
    } else {
      swal('Error', response.msg[2], 'error');
    }
  }, 'json');
}

/* elimina un registro de la base de datos */
function eliminar(rut_usuario) {
  swal({
    title: '¿Está seguro (a)?',
    text: "Esta operación no se puede revertir!",
    type: 'warning',
    showCancelButton: true,
  }).then(function () {
    $.ajax({
      dataType: 'json',
      async: true,
      url: '../modelo/usuario.php',
      data: {accion: 5, rut_usuario: rut_usuario},
      success: function (response) {    
        if (response.success) {          
          obtenerUsuario();
        } else {
          swal('Error', response.msg[2], 'error');
        }
      }, error: function (e) {
        swal('Error', e.responseText, 'error');
      }
    });    
  });
}

/* valida que los datos obligatorios tengan algún valor */
function validarFormularioUsuario() {
  if ($('#nombre_usuario').val().trim().length<1) {
    swal('Atención', "El Nombre es requerido", 'info');
    return false;
  }
  
  return true;
}
