<?php
if (session_status() == PHP_SESSION_NONE) {
  session_start();
}

setlocale (LC_TIME, "es_ES.utf8");

$file = "../../conf.ini";
$ini = parse_ini_file($file, false);

date_default_timezone_set($ini['timezone']);

function validarSesion() {
  if (isset($_SESSION["loggedinMIAPP"])) {
    if ($_SESSION["loggedinMIAPP"] != true) {
      logout();
    }
  } else {
    logout();
  }
}

function logout() {
  header('Location: ../lib/logout.php');
}

function encriptar($string)
{
    $encrypt_method = $GLOBALS['ini']['encrypt_method'];
    $secret_key = $GLOBALS['ini']['secret_key'];
    $secret_iv = $GLOBALS['ini']['secret_iv'];
    $encrypted = openssl_encrypt($string, $encrypt_method, $secret_key, 0, $secret_iv);
    $encrypted = base64_encode($encrypted);
    return $encrypted;
}

function desencriptar($string)
{
    $encrypt_method = $GLOBALS['ini']['encrypt_method'];
    $secret_key = $GLOBALS['ini']['secret_key'];
    $secret_iv = $GLOBALS['ini']['secret_iv'];
    $decrypted = openssl_decrypt(base64_decode($string), $encrypt_method, $secret_key, 0, $secret_iv);
    return $decrypted;
}


/*
 * devuelve ID conexión a la DB
 */
function conectarBD () {
  $i = $GLOBALS['ini'];
  
  $host = $i["host"];
  $db = $i["db"];
  $user = $i["user"]; 
  $password = $i["password"];
  $port = $i["port"];
  
  // conectar a la base de datos
  try {
    $conn = new PDO('pgsql:host='.$host.';port='.$port.';dbname='.$db.';user='.$user.';password='.$password);
    return $conn;
    
  } catch (PDOException $e) {
    //echo $e->getMessage();
    return false;
  }
}

function ejecutarSQL ($stmt) {
  $res = array();
  $res["success"] = false;
  $res["msg"] = "Error SQL";
  $res["data"] = null;
  
  try {
    if ($stmt->execute()) {
      $res["data"] = $stmt->fetchAll(PDO::FETCH_ASSOC);
      $res["msg"] = "éxito";
      $res["success"] = true;
      
    } else {
      
      /* https://www.postgresql.org/docs/12/errcodes-appendix.html */
      $arrayError['23503'] = "El registro no se puede eliminar porque tiene información asociada.";
      
      $res["msg"] = "Error SQL, Contacte al soporte";      
    }

  } catch (PDOException $e) {
    $res["msg"] = $e->getMessage();
  }
  
  return $res;
}

function head () {

$str = <<<EOF
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>{$GLOBALS['ini']['titulo']}</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" type="image/ico" href="../images/favicon.ico">

<link rel="stylesheet" href="../extras/bootstrap/bootstrap.css">
<!--<link rel="stylesheet" href="../extras/bootstrap/bootstrap-theme.min.css">-->
<link rel="stylesheet" href="../extras/sweetalert2/sweetalert2.min.css">
<link rel="stylesheet" href="../extras/jquery/jquery-ui.min.css">
<link rel="stylesheet" href="../css/common.css">
EOF;
echo $str;
}

function navbar () {
$strIn = <<<EOF
<nav class="navbar navbar-default">
  <div class="container-fluid">
  
    <div class="navbar-header">      
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#"><b>{$GLOBALS['ini']['brand']}</b></a>
    </div>
  
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      
      <ul class="nav navbar-nav">
        <li id="active_index">
          <a href="index.php"><span class="glyphicon glyphicon-home"></span> Inicio</a>
        </li>
      </ul>
      
      <ul class="nav navbar-nav">
        <li id="active_admin" class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#"><span class="glyphicon glyphicon-cog"></span> Administración<span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href=""><span class="glyphicon glyphicon-chevron-right"></span> Empresas</a></li>
            <li class="divider"></li>
            <li><a href=""><span class="glyphicon glyphicon-chevron-right"></span> Responsables</a></li>
          </ul>
        </li>
      </ul>
            
      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#"><span class="glyphicon glyphicon-user"></span> {$_SESSION['username']} <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="../lib/logout.php"><span class="glyphicon glyphicon-log-in"></span> Cerrar</a></li>
          </ul>
        </li>
      </ul>
      
    </div>
  </div>
</nav>
EOF;

$strOut = <<<EOF
<nav class="navbar navbar-default">
  <div class="container-fluid">
  
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#"><b>{$GLOBALS['ini']['brand']}</b></a>
    </div>
  
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <li>          
          <a href="" data-toggle="modal" data-target="#modal_login_popup"><span class="glyphicon glyphicon-log-in"></span> Entrar</a>
        </li>
      </ul>
    </div>
    
  </div>
</nav>
EOF;

  if ($_SESSION["loggedinMIAPP"] == true) {
    echo $strIn; 
  } else {
    echo $strOut; 
  }
}

function footer() {
$str = <<<EOF
<hr>
<div class="text-center">
  <a href="{$GLOBALS['ini']['url']}">{$GLOBALS['ini']['nombre']} - {$GLOBALS['ini']['url']}</a> 
  | <a href="mailto: {$GLOBALS['ini']['contacto']}">{$GLOBALS['ini']['contacto']}</a>
</div>

<script src="../extras/modernizr/modernizr-2.8.3.min.js"></script>
<script src="../extras/jquery/jquery-3.2.1.min.js"></script>
<script src="../extras/bootstrap/bootstrap.min.js"></script>
<script src="../extras/sweetalert2/sweetalert2.min.js"></script>
<script src="../extras/jquery/jquery-ui.min.js"></script>
<script src="../controller/common.js"></script>
EOF;
echo $str;
}


?>
