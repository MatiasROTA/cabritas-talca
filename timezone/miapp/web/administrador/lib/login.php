<?php
require_once("common.php");
?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
<?php 
  head(); 
?>
</head>
<body>
  <!--[if lt IE 8]>
  <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
  <![endif]-->
 
  <p></p>
  <div class="container-fluid">
    <!-- workspace -->

    <div class="row">
      <div align="center">
        <img src="../images/logo2.png" width="15%">
        <br/>

        <h1 class="text-default">
        <?php
          echo $GLOBALS['ini']['titulo'] . " " . $GLOBALS['ini']['version'];
        ?>
        </h1>

      </div>
    </div>
    
    <div class="row">
      <div class="col-md-4"></div>
      <div class="col-md-4">
        <p></p>         
        <div class="panel panel-primary">
          <div id="login_msg" class="panel-heading text-center">
              <!--<div id="info_login_msg" class="glyphicon glyphicon-chevron-right"></div>-->
              <label id="text_login_msg">Ingreso</label>
          </div>
        
          <div class="panel-body">
            <br>
            <form class="form-horizontal" id="formData" role="form" method="post">
              <div class="form-group">
                <label class="control-label col-sm-3" for="username">RUT </label>
                <div class="col-sm-9">
                  <input class="form-control" type="text" name="username" id="username" required placeholder="ej: 12345678K">
                 </div>
              </div>
              <div class="form-group">
                <label class="control-label col-sm-3" for="password">Contraseña </label>
                <div class="col-sm-9"> 
                  <input class="form-control" type="password" name="pwd" id="pwd" required>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-offset-3 col-sm-9">
                  <button class="btn btn-default" type="submit" id="btnsubmit" name="btnsubmit">Entrar</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  
  <?php footer(); ?>
  <script src="../controller/login.js"></script>
</body>
</html>
