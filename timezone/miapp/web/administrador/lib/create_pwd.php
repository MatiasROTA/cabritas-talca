<?php

$file = "../../conf.ini";
$ini = parse_ini_file($file, false);

function encriptar($string)
{
    $encrypt_method = $GLOBALS['ini']['encrypt_method'];
    $secret_key = $GLOBALS['ini']['secret_key'];
    $secret_iv = $GLOBALS['ini']['secret_iv'];
    $encrypted = openssl_encrypt($string, $encrypt_method, $secret_key, 0, $secret_iv);
    $encrypted = base64_encode($encrypted);
    return $encrypted;
}

function desencriptar($string)
{
    $encrypt_method = $GLOBALS['ini']['encrypt_method'];
    $secret_key = $GLOBALS['ini']['secret_key'];
    $secret_iv = $GLOBALS['ini']['secret_iv'];
    $decrypted = openssl_decrypt(base64_decode($string), $encrypt_method, $secret_key, 0, $secret_iv);
    return $decrypted;
}


$claveEncriptada = encriptar("admin");
echo $claveEncriptada;
echo "\n";

$original = desencriptar($claveEncriptada);
echo $original;
echo "\n";
?>
