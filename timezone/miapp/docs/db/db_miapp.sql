/*==============================================================*/
/* DBMS name:      PostgreSQL 8                                 */
/*==============================================================*/

drop index USUARIO_PK;

drop table USUARIO;

/*==============================================================*/
/* Table: USUARIO                                               */
/*==============================================================*/
create table USUARIO (
   ID_USU               VARCHAR(12)          not null,
   NOM_USU              VARCHAR(300)         not null,
   APEP_USU             VARCHAR(150)         not null,
   APEM_USU             VARCHAR(150)         null,
   EMAIL_USU            VARCHAR(400)         not null default 'n@mail.n',
   MOVIL_USU            VARCHAR(100)         not null default '955555555',
   CLAVE_USU            VARCHAR(512)         not null,
   NEMP                 INT4                 not null default 1,
   constraint PK_USUARIO primary key (ID_USU)
);

/*==============================================================*/
/* Index: USUARIO_PK                                            */
/*==============================================================*/
create unique index USUARIO_PK on USUARIO (
ID_USU
);
