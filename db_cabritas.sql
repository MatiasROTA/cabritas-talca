/*==============================================================*/
/* DBMS name:      PostgreSQL 8                                 */
/* Created on:     16-08-2020 23:07:57                          */
/*==============================================================*/


drop index CIUDAD_PK;

drop table CIUDAD;

drop index SE_ENCUENTRA_FK;

drop index PERTENECE_FK;

drop index DIRECCION_PK;

drop table DIRECCION;

drop index PRESENTA_FK;

drop index ENCARGA_FK;

drop index PEDIDO_PK;

drop table PEDIDO;

drop index PRODUCTO_CLIENTES_PK;

drop table PRODUCTO;

drop index SOLICITA_PRODUCTO_CLIENTES2_FK;

drop index SOLICITA_PRODUCTO_CLIENTES_FK;

drop index SOLICITA_PRODUCTO_CLIENTES_PK;

drop table SOLICITA_PRODUCTO_CLIENTES;

drop index TIPO_USUARIO_PK;

drop table TIPO_USUARIO;

drop index TIPO_FK;

drop index USUARIO_PK;

drop table USUARIO;

/*==============================================================*/
/* Table: CIUDAD                                                */
/*==============================================================*/
create table CIUDAD (
   ID_CIUDAD            SERIAL               not null,
   NOMBRE_CIUDAD        VARCHAR(50)          not null,
   constraint PK_CIUDAD primary key (ID_CIUDAD)
);

/*==============================================================*/
/* Index: CIUDAD_PK                                             */
/*==============================================================*/
create unique index CIUDAD_PK on CIUDAD (
ID_CIUDAD
);

/*==============================================================*/
/* Table: DIRECCION                                             */
/*==============================================================*/
create table DIRECCION (
   ID_DIRECCION         SERIAL               not null,
   RUT_USUARIO          INT4                 not null,
   ID_CIUDAD            INT4                 not null,
   CALLE                VARCHAR(150)         not null,
   NUMERO_CASA          INT4                 null,
   NUMERO_DEPARTAMENTO  INT4                 null,
   constraint PK_DIRECCION primary key (ID_DIRECCION)
);

/*==============================================================*/
/* Index: DIRECCION_PK                                          */
/*==============================================================*/
create unique index DIRECCION_PK on DIRECCION (
ID_DIRECCION
);

/*==============================================================*/
/* Index: PERTENECE_FK                                          */
/*==============================================================*/
create  index PERTENECE_FK on DIRECCION (
ID_CIUDAD
);

/*==============================================================*/
/* Index: SE_ENCUENTRA_FK                                       */
/*==============================================================*/
create  index SE_ENCUENTRA_FK on DIRECCION (
RUT_USUARIO
);

/*==============================================================*/
/* Table: PEDIDO                                                */
/*==============================================================*/
create table PEDIDO (
   ID_PEDIDO            SERIAL               not null,
   RUT_USUARIO          INT4                 not null,
   ID_DIRECCION         INT4                 not null,
   FECHA_PEDIDO         DATE                 not null,
   PRECIO_TOTAL_PEDIDO  INT4                 not null,
   ESTADO_PEDIDO        BOOL                 not null,
   DETALLE_PEDIDO       VARCHAR(300)         null,
   FECHA_ENTREGA_PEDIDO DATE                 not null,
   HORA_ENTREGA_PEDIDO  TIME                 not null,
   TIPO_ENTREGA         BOOL                 not null,
   constraint PK_PEDIDO primary key (ID_PEDIDO)
);

/*==============================================================*/
/* Index: PEDIDO_PK                                             */
/*==============================================================*/
create unique index PEDIDO_PK on PEDIDO (
ID_PEDIDO
);

/*==============================================================*/
/* Index: ENCARGA_FK                                            */
/*==============================================================*/
create  index ENCARGA_FK on PEDIDO (
RUT_USUARIO
);

/*==============================================================*/
/* Index: PRESENTA_FK                                           */
/*==============================================================*/
create  index PRESENTA_FK on PEDIDO (
ID_DIRECCION
);

/*==============================================================*/
/* Table: PRODUCTO                                              */
/*==============================================================*/
create table PRODUCTO (
   ID_PRODUCTO          SERIAL               not null,
   NOMBRE_PRODUCTO      VARCHAR(50)          not null,
   PRECIO_PRODUCTO_CLIENTE INT4                 null,
   GRAMOS_PRODUCTOS_CLIENTE INT4                 null,
   STOCK_PRODUCTOS_CLIENTE INT4                 null,
   PRECIO_PRODUCTO_SOCIO INT4                 null,
   GRAMOS_PRODUCTO_SOCIO INT4                 null,
   STOCK_PRODUCTO_SOCIO INT4                 null,
   DISPONIBILIDAD_SOCIO BOOL                 not null,
   DISPONIBILIDAD_CLIENTE BOOL                 not null,
   CALORIAS_PRODUCTO    INT4                 not null,
   URL_IMAGEN           VARCHAR(500)         not null,
   constraint PK_PRODUCTO primary key (ID_PRODUCTO)
);

/*==============================================================*/
/* Index: PRODUCTO_CLIENTES_PK                                  */
/*==============================================================*/
create unique index PRODUCTO_CLIENTES_PK on PRODUCTO (
ID_PRODUCTO
);

/*==============================================================*/
/* Table: SOLICITA_PRODUCTO_CLIENTES                            */
/*==============================================================*/
create table SOLICITA_PRODUCTO_CLIENTES (
   ID_PRODUCTO          INT4                 not null,
   ID_PEDIDO            INT4                 not null,
   CANTIDAD_PRODUCTO_CLIENTES INT4                 not null,
   constraint PK_SOLICITA_PRODUCTO_CLIENTES primary key (ID_PRODUCTO, ID_PEDIDO)
);

/*==============================================================*/
/* Index: SOLICITA_PRODUCTO_CLIENTES_PK                         */
/*==============================================================*/
create unique index SOLICITA_PRODUCTO_CLIENTES_PK on SOLICITA_PRODUCTO_CLIENTES (
ID_PRODUCTO,
ID_PEDIDO
);

/*==============================================================*/
/* Index: SOLICITA_PRODUCTO_CLIENTES_FK                         */
/*==============================================================*/
create  index SOLICITA_PRODUCTO_CLIENTES_FK on SOLICITA_PRODUCTO_CLIENTES (
ID_PRODUCTO
);

/*==============================================================*/
/* Index: SOLICITA_PRODUCTO_CLIENTES2_FK                        */
/*==============================================================*/
create  index SOLICITA_PRODUCTO_CLIENTES2_FK on SOLICITA_PRODUCTO_CLIENTES (
ID_PEDIDO
);

/*==============================================================*/
/* Table: TIPO_USUARIO                                          */
/*==============================================================*/
create table TIPO_USUARIO (
   ID_TIPO_CLIENTE      SERIAL               not null,
   NOMBRE_TIPO_CLIENTE  VARCHAR(50)          not null,
   constraint PK_TIPO_USUARIO primary key (ID_TIPO_CLIENTE)
);

/*==============================================================*/
/* Index: TIPO_USUARIO_PK                                       */
/*==============================================================*/
create unique index TIPO_USUARIO_PK on TIPO_USUARIO (
ID_TIPO_CLIENTE
);

/*==============================================================*/
/* Table: USUARIO                                               */
/*==============================================================*/
create table USUARIO (
   RUT_USUARIO          INT4                 not null,
   ID_TIPO_CLIENTE      INT4                 not null,
   NOMBRE_USUARIO       VARCHAR(50)          not null,
   APELLIDO_PATERNO     VARCHAR(50)          not null,
   APELLIDO_MATERNO     VARCHAR(50)          not null,
   TELEFONO_CLIENTE     INT4                 not null,
   CORREO_ELECTRONICO   VARCHAR(100)         not null,
   PASSWORD             VARCHAR(512)         not null,
   constraint PK_USUARIO primary key (RUT_USUARIO)
);

/*==============================================================*/
/* Index: USUARIO_PK                                            */
/*==============================================================*/
create unique index USUARIO_PK on USUARIO (
RUT_USUARIO
);

/*==============================================================*/
/* Index: TIPO_FK                                               */
/*==============================================================*/
create  index TIPO_FK on USUARIO (
ID_TIPO_CLIENTE
);

alter table DIRECCION
   add constraint FK_DIRECCIO_PERTENECE_CIUDAD foreign key (ID_CIUDAD)
      references CIUDAD (ID_CIUDAD)
      on delete restrict on update restrict;

alter table DIRECCION
   add constraint FK_DIRECCIO_SE_ENCUEN_USUARIO foreign key (RUT_USUARIO)
      references USUARIO (RUT_USUARIO)
      on delete restrict on update restrict;

alter table PEDIDO
   add constraint FK_PEDIDO_ENCARGA_USUARIO foreign key (RUT_USUARIO)
      references USUARIO (RUT_USUARIO)
      on delete restrict on update restrict;

alter table PEDIDO
   add constraint FK_PEDIDO_PRESENTA_DIRECCIO foreign key (ID_DIRECCION)
      references DIRECCION (ID_DIRECCION)
      on delete restrict on update restrict;

alter table SOLICITA_PRODUCTO_CLIENTES
   add constraint FK_SOLICITA_SOLICITA__PRODUCTO foreign key (ID_PRODUCTO)
      references PRODUCTO (ID_PRODUCTO)
      on delete restrict on update restrict;

alter table SOLICITA_PRODUCTO_CLIENTES
   add constraint FK_SOLICITA_SOLICITA__PEDIDO foreign key (ID_PEDIDO)
      references PEDIDO (ID_PEDIDO)
      on delete restrict on update restrict;

alter table USUARIO
   add constraint FK_USUARIO_TIPO_TIPO_USU foreign key (ID_TIPO_CLIENTE)
      references TIPO_USUARIO (ID_TIPO_CLIENTE)
      on delete restrict on update restrict;

